package com.android.obornik.filip.filmlover.ui.castDetail.castCredits;


import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityOptionsCompat;
import android.support.v4.app.Fragment;
import android.support.v4.util.Pair;
import android.support.v4.view.ViewCompat;
import android.support.v7.widget.CardView;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import butterknife.BindView;
import butterknife.ButterKnife;
import com.android.obornik.filip.filmlover.R;
import com.android.obornik.filip.filmlover.common.base.BaseFragment;
import com.android.obornik.filip.filmlover.common.presenter.BasePresenter;
import com.android.obornik.filip.filmlover.core.dagger.ActivityContext;
import com.android.obornik.filip.filmlover.data.model.castAndCrew.PersonDetail;
import com.android.obornik.filip.filmlover.data.model.credits.ActorsMovieCredit;
import com.android.obornik.filip.filmlover.data.model.movie.MovieSharedElement;
import com.android.obornik.filip.filmlover.ui.castDetail.CastDetailActivity;
import com.android.obornik.filip.filmlover.ui.movieDetail.MovieDetailActivity;
import com.android.obornik.filip.filmlover.utils.Constants;
import com.android.obornik.filip.filmlover.utils.Formatter;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.resource.drawable.DrawableTransitionOptions;
import com.bumptech.glide.request.RequestOptions;

import javax.inject.Inject;
import java.util.List;

import static com.bumptech.glide.request.RequestOptions.circleCropTransform;

/**
 * A simple {@link Fragment} subclass.
 */
public class CastCreditsFragment extends BaseFragment<CastCreditsFragmentContract.Presenter> implements CastCreditsFragmentContract.View {

    @BindView(R.id.movie_credits) RecyclerView recyclerViewMovieCredits;
    @BindView(R.id.loading) RelativeLayout loading;

    @Inject CreditsAdapter creditsAdapter;
    @Inject @ActivityContext Context context;

    private int castId;

    @Override
    public void showCastMovieCredits(List<ActorsMovieCredit> credits) {
        creditsAdapter.updateActorsMovieCredits(credits);
    }

    @Override
    public void showLoading() {
        recyclerViewMovieCredits.setVisibility(View.GONE);
        loading.setVisibility(View.VISIBLE);
    }

    @Override
    public void hideLoading() {
        loading.setVisibility(View.GONE);
        recyclerViewMovieCredits.setVisibility(View.VISIBLE);
    }

    @Override
    public void showMovieDetailActivity(MovieSharedElement movieSharedElement) {
        View moviePosterView = movieSharedElement.getViewPosterImage();
        View movieTitleView = movieSharedElement.getViewMovieTitle();

        Intent intent = new Intent(activityContext, MovieDetailActivity.class);
        intent.putExtra(MovieDetailActivity.EXTRA_MOVIE_ID, movieSharedElement.getMovieId());

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            intent.putExtra(MovieDetailActivity.EXTRA_MOVIE_POSTER_TRANSITION_NAME, ViewCompat.getTransitionName(moviePosterView));
            intent.putExtra(MovieDetailActivity.EXTRA_MOVIE_TITLE_TRANSITION_NAME, ViewCompat.getTransitionName(movieTitleView));
            Pair<View, String> moviePosterPair = Pair.create(moviePosterView, ViewCompat.getTransitionName(moviePosterView));
            Pair<View, String> movieTitlePair = Pair.create(movieTitleView, ViewCompat.getTransitionName(movieTitleView));
            ActivityOptionsCompat options = ActivityOptionsCompat.
                    makeSceneTransitionAnimation(this.getActivity(), moviePosterPair, movieTitlePair);
            startActivity(intent, options.toBundle());
        } else {
            startActivity(intent);
        }
    }

    public CastCreditsFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        if (getArguments() != null) {
            castId = getArguments().getInt(CastDetailActivity.EXTRA_CAST_ID);
        }
        return inflater.inflate(R.layout.fragment_cast_credits, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        ButterKnife.bind(this, view);
        recyclerViewMovieCredits.setLayoutManager(new LinearLayoutManager(context, LinearLayoutManager.VERTICAL, false));
        recyclerViewMovieCredits.setAdapter(creditsAdapter);
        presenter.attachView(this);
        creditsAdapter.getItemClickObservable().subscribe(movieSharedElement -> presenter.onCreditItemClicked(movieSharedElement));
        presenter.loadCastMovieCredits(castId);
    }
}
