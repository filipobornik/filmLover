package com.android.obornik.filip.filmlover.ui.registerOrSignIn;

/**
 * Created by Filip on 9/12/2017.
 */

public interface RegisterOrSignInContract {

    interface View {

        void showTextMessage(String message);

    }

    interface Presenter {

        void onSignInBtnClick();

        void onUseAsGuestBtnClick();

        void onRegisterTextClick();

    }

}
