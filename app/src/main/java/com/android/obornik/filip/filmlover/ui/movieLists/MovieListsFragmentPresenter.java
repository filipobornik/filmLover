package com.android.obornik.filip.filmlover.ui.movieLists;

import com.android.obornik.filip.filmlover.core.dagger.PerFragment;

import javax.inject.Inject;

/**
 * Created by Filip on 12/28/2017.
 */
@PerFragment
public class MovieListsFragmentPresenter implements MovieListsFragmentContract.Presenter {

    private MovieListsFragmentContract.View view;

    @Inject
    public MovieListsFragmentPresenter() {

    }

    @Override
    public void attachView(MovieListsFragmentContract.View view) {
        this.view = view;
    }

    @Override
    public void detachView() {
        this.view = null;
    }

    @Override
    public boolean isViewAttached() {
        return view != null;
    }
}
