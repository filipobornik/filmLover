package com.android.obornik.filip.filmlover.ui.movieDetail;

import com.android.obornik.filip.filmlover.core.dagger.PerActivity;
import dagger.Binds;
import dagger.Module;

/**
 * Created by Filip on 1/8/2018.
 */

@Module
public abstract class MovieDetailActivityPresenterModule {

    @Binds
    @PerActivity
    abstract MovieDetailActivityContract.Presenter providePresenter(MovieDetailActivityPresenter movieDetailActivityPresenter);


}
