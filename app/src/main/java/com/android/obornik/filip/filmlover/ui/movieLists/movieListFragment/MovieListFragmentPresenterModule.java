package com.android.obornik.filip.filmlover.ui.movieLists.movieListFragment;

import com.android.obornik.filip.filmlover.core.dagger.PerChildFragment;
import dagger.Binds;
import dagger.Module;
import dagger.Provides;
import io.reactivex.disposables.CompositeDisposable;

/**
 * Created by Filip on 12/28/2017.
 */

@Module
public abstract class MovieListFragmentPresenterModule {

    @PerChildFragment
    @Binds
    abstract MovieListFragmentContract.Presenter provideBaseMovieListFragmentPresenter(MovieListFragmentPresenter presenter);

    @PerChildFragment
    @Provides
    static CompositeDisposable provideCompositeDisposable() {
        return new CompositeDisposable();
    }

}
