package com.android.obornik.filip.filmlover.common.base;

import dagger.Module;

/**
 * Created by Filip on 12/1/2017.
 */

@Module
public abstract class BaseChildFragmentModule {

    public static final String CHILD_FRAGMENT = "BaseChildFragmentModule.childFragment";

}
