package com.android.obornik.filip.filmlover.ui.castDetail.castCredits;

import android.content.Context;
import android.os.Build;
import android.support.v4.view.ViewCompat;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import at.grabner.circleprogress.CircleProgressView;
import butterknife.BindView;
import butterknife.ButterKnife;
import com.android.obornik.filip.filmlover.R;
import com.android.obornik.filip.filmlover.core.dagger.ActivityContext;
import com.android.obornik.filip.filmlover.core.dagger.PerActivity;
import com.android.obornik.filip.filmlover.data.model.credits.ActorsMovieCredit;
import com.android.obornik.filip.filmlover.data.model.movie.MovieSharedElement;
import com.android.obornik.filip.filmlover.utils.Constants;
import com.android.obornik.filip.filmlover.utils.Formatter;
import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import io.reactivex.subjects.PublishSubject;

import javax.inject.Inject;
import java.util.ArrayList;
import java.util.List;

@PerActivity
public class CreditsAdapter extends RecyclerView.Adapter<CreditsAdapter.MyViewHolder> {

    private PublishSubject<MovieSharedElement> itemClickObservable;
    private List<ActorsMovieCredit> actorsMovieCredits;
    @ActivityContext private Context context;

    @Inject
    public CreditsAdapter(@ActivityContext Context context) {
        this.context = context;
        this.actorsMovieCredits = new ArrayList<>();
        itemClickObservable = PublishSubject.create();
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.credit_item, parent, false);
        return new MyViewHolder(view);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {
        ActorsMovieCredit actorsMovieCredit = actorsMovieCredits.get(position);
        holder.txtViewMovieTitle.setText(actorsMovieCredit.getTitle());
        holder.txtViewCharactersName.setText(actorsMovieCredit.getCharacter());
        holder.txtViewReleaseDate.setText(Formatter.formatDateFromDatabase(actorsMovieCredit.getReleaseDate()));
        holder.reviewBar.setValue(new Float(actorsMovieCredit.getVoteAverage()));
        Glide.with(context)
                .load(Constants.BASE_POSTER_URL + actorsMovieCredit.getPosterPath())
                .apply(RequestOptions.centerCropTransform())
                .into(holder.imgMoviePoster);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            ViewCompat.setTransitionName(holder.imgMoviePoster, actorsMovieCredit.getPosterPath());
            ViewCompat.setTransitionName(holder.txtViewMovieTitle, actorsMovieCredit.getTitle());
        }

    }

    @Override
    public int getItemCount() {
        return actorsMovieCredits.size();
    }

    public void updateActorsMovieCredits(List<ActorsMovieCredit> actorsMovieCredits) {
        this.actorsMovieCredits = actorsMovieCredits;
        notifyDataSetChanged();
    }

    class MyViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.release_date) TextView txtViewReleaseDate;
        @BindView(R.id.movie_title) TextView txtViewMovieTitle;
        @BindView(R.id.review_bar) CircleProgressView reviewBar;
        @BindView(R.id.characters_name) TextView txtViewCharactersName;
        @BindView(R.id.movie_poster) ImageView imgMoviePoster;

        public MyViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
            itemView.setOnClickListener(v -> movieSelected(actorsMovieCredits.get(getAdapterPosition()).getId(), imgMoviePoster, txtViewMovieTitle));
        }

    }

    private void movieSelected(Integer id, ImageView imgMoviePoster, TextView txtViewMovieTitle) {
        itemClickObservable.onNext(new MovieSharedElement(id, imgMoviePoster, txtViewMovieTitle));
    }

    public PublishSubject<MovieSharedElement> getItemClickObservable() {
        return itemClickObservable;
    }
}
