package com.android.obornik.filip.filmlover.ui.movieDetail;

import com.android.obornik.filip.filmlover.core.dagger.PerActivity;
import com.android.obornik.filip.filmlover.data.DataManager;
import com.android.obornik.filip.filmlover.utils.schedulers.SchedulersProvider;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.schedulers.Schedulers;

import javax.inject.Inject;

/**
 * Created by Filip on 1/8/2018.
 */

@PerActivity
public class MovieDetailActivityPresenter implements MovieDetailActivityContract.Presenter {

    private MovieDetailActivityContract.View view;
    private DataManager dataManager;
    private SchedulersProvider schedulersProvider;
    private CompositeDisposable compositeDisposable;


    @Inject
    public MovieDetailActivityPresenter(DataManager dataManager, SchedulersProvider schedulers) {
        this.dataManager = dataManager;
        this.schedulersProvider = schedulers;
        this.compositeDisposable = new CompositeDisposable();
    }

    @Override
    public void attachView(MovieDetailActivityContract.View view) {
        this.view = view;
    }

    @Override
    public void detachView() {
        this.view = null;
    }

    @Override
    public boolean isViewAttached() {
        return view != null;
    }

    @Override
    public void loadMovieDetails(int movieId) {
        compositeDisposable.add(dataManager.getMovieDetails(movieId)
                .subscribe(movieDetail -> {
                    if (isViewAttached()) {
                        view.showMovieDetails(movieDetail);
                    }
                })
        );
    }


}
