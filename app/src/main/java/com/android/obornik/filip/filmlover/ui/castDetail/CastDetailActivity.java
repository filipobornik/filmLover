package com.android.obornik.filip.filmlover.ui.castDetail;

import android.os.Bundle;
import android.support.design.widget.CollapsingToolbarLayout;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.widget.*;
import butterknife.BindView;
import butterknife.ButterKnife;
import com.android.obornik.filip.filmlover.R;
import com.android.obornik.filip.filmlover.common.base.BaseActivity;
import com.android.obornik.filip.filmlover.data.model.castAndCrew.PersonDetail;
import com.android.obornik.filip.filmlover.utils.Constants;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.resource.drawable.DrawableTransitionOptions;
import com.bumptech.glide.request.RequestOptions;
import dagger.android.AndroidInjector;
import dagger.android.DispatchingAndroidInjector;
import dagger.android.support.HasSupportFragmentInjector;

import javax.inject.Inject;

import static com.bumptech.glide.request.RequestOptions.circleCropTransform;

public class CastDetailActivity extends BaseActivity<CastDetailActivityContract.Presenter> implements CastDetailActivityContract.View, HasSupportFragmentInjector {

    private static final String LOG_TAG = CastDetailActivity.class.getSimpleName();
    public static String EXTRA_CAST_ID = "extraCastId";

    @BindView(R.id.toolbar) Toolbar toolbar;
    @BindView(R.id.person_image) ImageView imgViewPersonImage;
    @BindView(R.id.collapsing_toolbar_layout) CollapsingToolbarLayout collapsingToolbarLayout;
    @BindView(R.id.viewPager) ViewPager viewPager;
    @BindView(R.id.tabs) TabLayout tabLayout;

    @Inject ViewPagerAdapter viewPagerAdapter;
    @Inject DispatchingAndroidInjector<Fragment> fragmentInjector;

    private int castId = -1;


    @Override
    public void showCastDetail(PersonDetail personDetail) {
        collapsingToolbarLayout.setTitle(personDetail.getName());
        Glide.with(this)
                .load(Constants.BASE_PERSON_IMAGE_URL + personDetail.getProfilePath())
                .apply(RequestOptions.placeholderOf(R.drawable.default_actor_image_round))
                .transition(DrawableTransitionOptions.withCrossFade())
                .apply(circleCropTransform())
                .into(imgViewPersonImage);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_cast_detail);
        ButterKnife.bind(this);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

        castId = getIntent().getIntExtra(EXTRA_CAST_ID, -1);
        viewPagerAdapter.setPersonsId(castId);
        viewPager.setAdapter(viewPagerAdapter);
        tabLayout.setupWithViewPager(viewPager);
        presenter.attachView(this);

        if (castId != -1) {
            presenter.loadCastDetails(castId);
        } else {
            // show some error message
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;
        }

        return (super.onOptionsItemSelected(item));
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        presenter.detachView();
    }

    @Override
    public AndroidInjector<Fragment> supportFragmentInjector() {
        return fragmentInjector;
    }
}
