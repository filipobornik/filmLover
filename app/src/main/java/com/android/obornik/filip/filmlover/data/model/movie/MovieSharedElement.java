package com.android.obornik.filip.filmlover.data.model.movie;

import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

/**
 * Created by Filip on 1/10/2018.
 */

public class MovieSharedElement {

    private int movieId;
    private View viewPosterImage;
    private View viewMovieTitle;

    public MovieSharedElement(int movieId, View viewPosterImage, View viewTitle) {
        this.movieId = movieId;
        this.viewPosterImage = viewPosterImage;
        this.viewMovieTitle = viewTitle;
    }

    public int getMovieId() {
        return movieId;
    }

    public void setMovieId(int movieId) {
        this.movieId = movieId;
    }

    public View getViewPosterImage() {
        return viewPosterImage;
    }

    public void setViewPosterImage(View viewPosterImage) {
        this.viewPosterImage = viewPosterImage;
    }

    public View getViewMovieTitle() {
        return viewMovieTitle;
    }

    public void setViewMovieTitle(View viewMovieTitle) {
        this.viewMovieTitle = viewMovieTitle;
    }
}
