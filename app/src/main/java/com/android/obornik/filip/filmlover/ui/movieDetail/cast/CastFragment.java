package com.android.obornik.filip.filmlover.ui.movieDetail.cast;


import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import butterknife.BindView;
import butterknife.ButterKnife;
import com.android.obornik.filip.filmlover.R;
import com.android.obornik.filip.filmlover.common.base.BaseFragment;
import com.android.obornik.filip.filmlover.core.dagger.ActivityContext;
import com.android.obornik.filip.filmlover.data.model.castAndCrew.Cast;
import com.android.obornik.filip.filmlover.ui.castDetail.CastDetailActivity;
import com.android.obornik.filip.filmlover.ui.movieDetail.MovieDetailActivity;

import javax.inject.Inject;
import java.util.List;

/**
 * A simple {@link Fragment} subclass.
 */
public class CastFragment extends BaseFragment<CastFragmentContract.Presenter> implements CastFragmentContract.View {

    @BindView(R.id.cast_recycler_view) RecyclerView recyclerView;
    @Inject CastListAdapter castListAdapter;
    @Inject @ActivityContext Context context;

    private int movieId = 0;

    public CastFragment() {
        // Required empty public constructor
    }

    @Override
    public void showCast(List<Cast> cast) {
        castListAdapter.updateCastList(cast);
    }

    @Override
    public void showActorDetail(int castId) {
        Intent intent = new Intent(this.getContext(), CastDetailActivity.class);
        intent.putExtra(CastDetailActivity.EXTRA_CAST_ID, castId);
        startActivity(intent);
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_cast, container, false);
        ButterKnife.bind(this, view);
        presenter.attachView(this);
        recyclerView.setLayoutManager(new LinearLayoutManager(this.getContext(), LinearLayoutManager.VERTICAL, false));
        recyclerView.setAdapter(castListAdapter);
        recyclerView.addItemDecoration(new DividerItemDecoration(context, DividerItemDecoration.HORIZONTAL));
        if (getArguments() != null) {
            movieId = getArguments().getInt(MovieDetailActivity.EXTRA_MOVIE_ID);
        }
        return view;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        presenter.loadCast(movieId, 1);
        castListAdapter.getItemOnClickObservable().subscribe(castId -> {
            presenter.onCastItemClicked(castId);
        });
    }


    @Override
    public void onDestroy() {
        super.onDestroy();
        presenter.detachView();
    }
}
