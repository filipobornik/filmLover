package com.android.obornik.filip.filmlover.ui.movieDetail.cast;

import com.android.obornik.filip.filmlover.core.dagger.PerFragment;
import com.android.obornik.filip.filmlover.data.DataManager;

import javax.inject.Inject;

/**
 * Created by Filip on 1/29/2018.
 */

@PerFragment
public class CastFragmentPresenter implements CastFragmentContract.Presenter {

    private CastFragmentContract.View view;
    DataManager dataManager;

    @Inject
    public CastFragmentPresenter(DataManager dataManager) {
        this.dataManager = dataManager;
    }

    @Override
    public void attachView(CastFragmentContract.View view) {
        this.view = view;
    }

    @Override
    public void detachView() {
        this.view = null;
    }

    @Override
    public boolean isViewAttached() {
        return view != null;
    }

    @Override
    public void loadCast(int movieId, int pages) {
        dataManager.getCast(movieId, pages).subscribe(cast -> {
            if (isViewAttached()) {
                view.showCast(cast);
            }
        });
    }

    @Override
    public void onCastItemClicked(int castId) {
        view.showActorDetail(castId);
    }
}
