package com.android.obornik.filip.filmlover.ui.movieDetail;

import com.android.obornik.filip.filmlover.common.presenter.BasePresenter;
import com.android.obornik.filip.filmlover.common.view.BaseView;
import com.android.obornik.filip.filmlover.data.model.movie.MovieDetail;

public class MovieDetailActivityContract {

    interface View extends BaseView {
        void showMovieDetails(MovieDetail movie);
    }


    interface Presenter extends BasePresenter<View> {
        void loadMovieDetails(int movieId);

    }

}
