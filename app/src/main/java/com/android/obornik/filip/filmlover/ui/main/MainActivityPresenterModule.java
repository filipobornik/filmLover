package com.android.obornik.filip.filmlover.ui.main;

import com.android.obornik.filip.filmlover.core.dagger.PerActivity;
import dagger.Binds;
import dagger.Module;

/**
 * Created by Filip on 12/20/2017.
 */

@Module
public abstract class MainActivityPresenterModule {

    @Binds
    @PerActivity
    abstract MainActivityContract.Presenter provideMainActivityPresenter(MainActivityPresenter presenter);

}
