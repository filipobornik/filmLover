package com.android.obornik.filip.filmlover.data.test;

import com.android.obornik.filip.filmlover.data.model.movie.Movie;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * Created by Filip on 9/22/2017.
 */

public class TempRepository {

    List<Integer> genre_ids = Arrays.asList(10751, 16, 12, 35);

    private static ArrayList<Movie> movies = new ArrayList<Movie>(Arrays.asList(
            new Movie(4578, 211672, false, 6.4, "Minions", 898.484189, "/q0R4crx2SehcEEQEkYObktdeFy.jpg","en","Minions", Arrays.asList(10751, 16, 12, 35),
                    "/uX7LXnsC7bZJZjn048UCOwkPXWJ.jpg", false, "Minions Stuart, Kevin and Bob are recruited by Scarlet Overkill," +
                    " a super-villain who, alongside her inventor husband Herb, hatches a plot to take over the world.", "2015-06-17"),
            new Movie(5309, 321612, false, 6.8, "Beauty and the Beast", 649.962033, "/tWqifoYuwLETmmasnGHO7xBjEtt.jpg", "en",
                    "Beauty and the Beast", Arrays.asList(10751, 14, 10749), "/6aUWe0GSl69wMTSWWexsorMIvwU.jpg", false,
                    "A live-action adaptation of Disney's version of the classic 'Beauty and the Beast' tale of" +
                            " a cursed prince and a beautiful young woman who helps him break the spell.", "2017-03-16"),
            new Movie(4578, 211672, false, 6.4, "Minions", 898.484189, "/q0R4crx2SehcEEQEkYObktdeFy.jpg","en","Minions", Arrays.asList(10751, 16, 12, 35),
                    "/uX7LXnsC7bZJZjn048UCOwkPXWJ.jpg", false, "Minions Stuart, Kevin and Bob are recruited by Scarlet Overkill," +
                    " a super-villain who, alongside her inventor husband Herb, hatches a plot to take over the world.", "2015-06-17"),
            new Movie(5309, 321612, false, 1.5, "Beauty and the Beast", 649.962033, "/tWqifoYuwLETmmasnGHO7xBjEtt.jpg", "en",
                    "Beauty and the Beast", Arrays.asList(10751, 14, 10749), "/6aUWe0GSl69wMTSWWexsorMIvwU.jpg", false,
                    "A live-action adaptation of Disney's version of the classic 'Beauty and the Beast' tale of" +
                            " a cursed prince and a beautiful young woman who helps him break the spell.", "2017-03-16"),
            new Movie(4578, 211672, false, 9.4, "Minions", 898.484189, "/q0R4crx2SehcEEQEkYObktdeFy.jpg","en","Minions", Arrays.asList(10751, 16, 12, 35),
                    "/uX7LXnsC7bZJZjn048UCOwkPXWJ.jpg", false, "Minions Stuart, Kevin and Bob are recruited by Scarlet Overkill," +
                    " a super-villain who, alongside her inventor husband Herb, hatches a plot to take over the world.", "2015-06-17"),
            new Movie(5309, 321612, false, 3.2, "Beauty and the Beast", 649.962033, "/tWqifoYuwLETmmasnGHO7xBjEtt.jpg", "en",
                    "Beauty and the Beast", Arrays.asList(10751, 14, 10749), "/6aUWe0GSl69wMTSWWexsorMIvwU.jpg", false,
                    "A live-action adaptation of Disney's version of the classic 'Beauty and the Beast' tale of" +
                            " a cursed prince and a beautiful young woman who helps him break the spell.", "2017-03-16"),
            new Movie(4578, 211672, false, 4., "Minions", 898.484189, "/q0R4crx2SehcEEQEkYObktdeFy.jpg","en","Minions", Arrays.asList(10751, 16, 12, 35),
                    "/uX7LXnsC7bZJZjn048UCOwkPXWJ.jpg", false, "Minions Stuart, Kevin and Bob are recruited by Scarlet Overkill," +
                    " a super-villain who, alongside her inventor husband Herb, hatches a plot to take over the world.", "2015-06-17"),
            new Movie(5309, 321612, false, 5.3, "Beauty and the Beast", 649.962033, "/tWqifoYuwLETmmasnGHO7xBjEtt.jpg", "en",
                    "Beauty and the Beast", Arrays.asList(10751, 14, 10749), "/6aUWe0GSl69wMTSWWexsorMIvwU.jpg", false,
                    "A live-action adaptation of Disney's version of the classic 'Beauty and the Beast' tale of" +
                            " a cursed prince and a beautiful young woman who helps him break the spell.", "2017-03-16"),
            new Movie(4578, 211672, false, 7.1, "Minions", 898.484189, "/q0R4crx2SehcEEQEkYObktdeFy.jpg","en","Minions", Arrays.asList(10751, 16, 12, 35),
                    "/uX7LXnsC7bZJZjn048UCOwkPXWJ.jpg", false, "Minions Stuart, Kevin and Bob are recruited by Scarlet Overkill," +
                    " a super-villain who, alongside her inventor husband Herb, hatches a plot to take over the world.", "2015-06-17"),
            new Movie(5309, 321612, false, 10., "Beauty and the Beast", 649.962033, "/tWqifoYuwLETmmasnGHO7xBjEtt.jpg", "en",
                    "Beauty and the Beast", Arrays.asList(10751, 14, 10749), "/6aUWe0GSl69wMTSWWexsorMIvwU.jpg", false,
                    "A live-action adaptation of Disney's version of the classic 'Beauty and the Beast' tale of" +
                            " a cursed prince and a beautiful young woman who helps him break the spell.", "2017-03-16")));

    public static ArrayList<Movie> getTempMovies() {
        return movies;
    }
}






