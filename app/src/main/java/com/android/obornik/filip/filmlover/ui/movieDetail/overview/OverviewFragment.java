package com.android.obornik.filip.filmlover.ui.movieDetail.overview;


import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import android.widget.TextView;
import at.grabner.circleprogress.CircleProgressView;
import butterknife.BindView;
import butterknife.ButterKnife;
import com.android.obornik.filip.filmlover.R;
import com.android.obornik.filip.filmlover.common.base.BaseFragment;
import com.android.obornik.filip.filmlover.data.model.movie.MovieDetail;
import com.android.obornik.filip.filmlover.ui.movieDetail.MovieDetailActivity;

/**
 * A simple {@link Fragment} subclass.
 */
public class OverviewFragment extends BaseFragment<OverviewFragmentContract.Presenter> implements OverviewFragmentContract.View {

    private int movieId;

    @BindView(R.id.circleProgressView) CircleProgressView circleProgressView;
    @BindView(R.id.overview) TextView txtViewOverview;
    @BindView(R.id.company) TextView txtViewCompany;
    @BindView(R.id.budget) TextView txtViewBudget;
    @BindView(R.id.revenue) TextView txtViewRevenue;
    @BindView(R.id.status) TextView txtViewStatus;
    @BindView(R.id.reviews_count) TextView txtViewReviewsCount;

    public OverviewFragment() {
        // Required empty public constructor
    }

    @Override
    public void showMovieOverview(MovieDetail movieDetail) {
        txtViewOverview.setText(movieDetail.getOverview());
        txtViewReviewsCount.setText(movieDetail.getVoteCount().toString());
        circleProgressView.setValue(movieDetail.getVoteAverage().floatValue()*10);
        if (!movieDetail.getProductionCompanies().isEmpty()) {
            txtViewCompany.setText(movieDetail.getProductionCompanies().get(0).getName());
        } else {
            txtViewCompany.setText("-");
        }
        String budget = movieDetail.getBudget() == 0 ? "-" : "$ " + movieDetail.getBudget();
        txtViewBudget.setText(budget);
        String revenue = movieDetail.getRevenue() == 0 ? "-" : "$ " + movieDetail.getRevenue();
        txtViewRevenue.setText(revenue);
        String status = movieDetail.getStatus().isEmpty() ? "-" : movieDetail.getStatus();
        txtViewStatus.setText(status);
    }


    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_overview, container, false);
        ButterKnife.bind(this, view);
        if (getArguments() != null) {
            movieId = getArguments().getInt(MovieDetailActivity.EXTRA_MOVIE_ID);
        }
        return view;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        presenter.attachView(this);
        presenter.loadMovieOverview(movieId);
    }

    @Override
    public void onDestroy() {
        presenter.detachView();
        super.onDestroy();
    }
}
