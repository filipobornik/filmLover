package com.android.obornik.filip.filmlover.common.base;

import android.app.Activity;
import android.content.Context;
import android.content.res.Resources;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.IdRes;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.util.Log;
import android.widget.Toast;
import butterknife.ButterKnife;
import butterknife.Unbinder;
import com.android.obornik.filip.filmlover.R;
import com.android.obornik.filip.filmlover.common.presenter.BasePresenter;
import com.android.obornik.filip.filmlover.common.view.BaseView;
import dagger.android.AndroidInjector;
import dagger.android.DispatchingAndroidInjector;
import dagger.android.HasFragmentInjector;
import dagger.android.support.AndroidSupportInjection;
import dagger.android.support.HasSupportFragmentInjector;

import javax.inject.Inject;
import javax.inject.Named;

/**
 * Created by Filip on 12/1/2017.
 */

public class BaseFragment<T extends BasePresenter> extends Fragment implements BaseView, HasSupportFragmentInjector {

    private static final String LOG_TAG = BaseFragment.class.getSimpleName();
    @Nullable private Unbinder unbinder;

    @Inject protected Context activityContext;
    @Inject protected Resources resources;
    @Inject @Named(BaseFragmentModule.CHILD_FRAGMENT_MANAGER) protected FragmentManager childFragmentManager;
    @Inject protected T presenter;
    @Inject DispatchingAndroidInjector<Fragment> childFragmentInjector;

    @Override
    public void onAttach(Context context) {
        AndroidSupportInjection.inject(this);
        Log.d(LOG_TAG, "Base fragment has been recreated");
        super.onAttach(context);
    }

    @Override
    public void onViewStateRestored(@Nullable Bundle savedInstanceState) {
        super.onViewStateRestored(savedInstanceState);
        unbinder = ButterKnife.bind(this, getView());
    }

    @Override
    public void onDestroyView() {
        if (unbinder != null) {
            unbinder.unbind();
        }

        super.onDestroyView();
    }

    @Override
    public void showMessage(String message) {
        Toast.makeText(activityContext, message, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void showMessage(int messageResourceId) {
        Toast.makeText(activityContext, resources.getText(messageResourceId), Toast.LENGTH_SHORT).show();
    }

    @Override
    public void showError(String error) {
        Toast.makeText(activityContext, error, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void showError(int messageResourceId) {
        Toast.makeText(activityContext, resources.getText(messageResourceId), Toast.LENGTH_SHORT).show();
    }

    @Override
    public AndroidInjector<Fragment> supportFragmentInjector() {
        return childFragmentInjector;
    }

    @Override
    public void addFragment(@IdRes int resId, Fragment fragment) {
        childFragmentManager.beginTransaction()
                .add(resId, fragment)
                .commitAllowingStateLoss();
    }
}
