package com.android.obornik.filip.filmlover.data.remote;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import com.android.obornik.filip.filmlover.core.dagger.ActivityContext;
import com.android.obornik.filip.filmlover.core.dagger.ApplicationContext;

import javax.inject.Inject;
import javax.inject.Singleton;

/**
 * Created by Filip on 1/3/2018.
 */

@Singleton
public class NetworkMonitor {

    private final Context context;

    @Inject
    public NetworkMonitor(@ApplicationContext Context context) {
        this.context = context;
    }

    public boolean isConnected() {
        ConnectivityManager connectivityManager = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo networkInfo = connectivityManager.getActiveNetworkInfo();
        return networkInfo != null && networkInfo.isConnectedOrConnecting();
    }
}
