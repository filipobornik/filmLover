package com.android.obornik.filip.filmlover.utils.customExceptions;

import java.io.IOException;

/**
 * Created by Filip on 1/3/2018.
 */

public class NoNetworkExceptions extends IOException {
    public NoNetworkExceptions() {
        super("No network");
    }
}
