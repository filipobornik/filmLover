package com.android.obornik.filip.filmlover.ui.movieLists.movieListFragment;

import android.support.v4.app.Fragment;
import com.android.obornik.filip.filmlover.common.base.BaseChildFragmentModule;
import com.android.obornik.filip.filmlover.core.dagger.PerChildFragment;
import dagger.Binds;
import dagger.Module;

import javax.inject.Named;

/**
 * Created by Filip on 12/20/2017.
 */

@Module(includes = {BaseMovieListFragmentModule.class})
public abstract class MovieListFragmentModule {

    @Binds
    @Named(BaseChildFragmentModule.CHILD_FRAGMENT)
    @PerChildFragment
    abstract Fragment fragment(MovieListFragment movieListFragmnet);

    @Binds
    @Named(BaseChildFragmentModule.CHILD_FRAGMENT)
    @PerChildFragment
    abstract MovieListFragmentContract.View provideFragmentView(MovieListFragment movieListFragmnet);

}
