package com.android.obornik.filip.filmlover.ui.movieDetail;

import android.content.Context;
import android.support.v7.app.AppCompatActivity;
import com.android.obornik.filip.filmlover.common.base.BaseActivityModule;
import com.android.obornik.filip.filmlover.core.dagger.ActivityContext;
import com.android.obornik.filip.filmlover.core.dagger.PerActivity;
import com.android.obornik.filip.filmlover.core.dagger.PerFragment;
import com.android.obornik.filip.filmlover.ui.movieDetail.cast.CastFragment;
import com.android.obornik.filip.filmlover.ui.movieDetail.cast.CastFragmentModule;
import com.android.obornik.filip.filmlover.ui.movieDetail.overview.OverviewFragment;
import com.android.obornik.filip.filmlover.ui.movieDetail.overview.OverviewFragmentModule;
import com.android.obornik.filip.filmlover.ui.movieDetail.reviews.ReviewsFragment;
import com.android.obornik.filip.filmlover.ui.movieDetail.reviews.ReviewsFragmentModule;
import dagger.Binds;
import dagger.Module;
import dagger.android.ContributesAndroidInjector;

/**
 * Created by Filip on 1/8/2018.
 */

@Module(includes = {BaseActivityModule.class, MovieDetailActivityPresenterModule.class})
public abstract class MovieDetailActivityModule {

    @PerFragment
    @ContributesAndroidInjector(modules = OverviewFragmentModule.class)
    abstract OverviewFragment overviewFragment();

    @PerFragment
    @ContributesAndroidInjector(modules = ReviewsFragmentModule.class)
    abstract ReviewsFragment reviewsFragment();

    @PerFragment
    @ContributesAndroidInjector(modules = CastFragmentModule.class)
    abstract CastFragment castFragment();

    @Binds
    @PerActivity
    abstract AppCompatActivity provideMovieDetailActivity(MovieDetailActivity movieDetailActivity);

    @Binds
    @PerActivity
    abstract MovieDetailActivityContract.View provideMovieDetailActivityAsView(MovieDetailActivity movieDetailActivity);

    @Binds
    @ActivityContext
    @PerActivity
    abstract Context provideMovieDetailActivityContext(MovieDetailActivity movieDetailActivity);

//    @Binds
//    @PerActivity
//    abstract FragmentStatePagerAdapter provideFragmentStatePagerAdapter(ViewPagerAdapter viewPagerAdapter);
}
