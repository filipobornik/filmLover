package com.android.obornik.filip.filmlover.ui.movieDetail;

import android.content.res.Resources;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import com.android.obornik.filip.filmlover.R;
import com.android.obornik.filip.filmlover.common.base.BaseActivityModule;
import com.android.obornik.filip.filmlover.core.dagger.PerActivity;
import com.android.obornik.filip.filmlover.ui.movieDetail.cast.CastFragment;
import com.android.obornik.filip.filmlover.ui.movieDetail.crew.CrewFragment;
import com.android.obornik.filip.filmlover.ui.movieDetail.overview.OverviewFragment;
import com.android.obornik.filip.filmlover.ui.movieDetail.reviews.ReviewsFragment;

import javax.inject.Inject;
import javax.inject.Named;

/**
 * Created by Filip on 1/13/2018.
 */

@PerActivity
public class ViewPagerAdapter extends FragmentStatePagerAdapter {
    private Resources resources;
    private int movieId;
    private Bundle bundle;

    @Inject
    public ViewPagerAdapter(@Named(BaseActivityModule.ACTIVITY_FRAGMENT_MANAGER) FragmentManager fm, Resources resources) {
        super(fm);
        this.resources = resources;
        bundle = new Bundle();
    }

    @Override
    public Fragment getItem(int position) {
        switch (position) {
            case 0:
                OverviewFragment overviewFragment = new OverviewFragment();
                overviewFragment.setArguments(bundle);
                return overviewFragment;
            case 1:
                ReviewsFragment reviewsFragment = new ReviewsFragment();
                reviewsFragment.setArguments(bundle);
                return reviewsFragment;
            case 2:
                CastFragment castFragment = new CastFragment();
                castFragment.setArguments(bundle);
                return castFragment;
            default:
                return new CrewFragment();
        }
    }

    @Override
    public CharSequence getPageTitle(int position) {
        switch (position) {
            case 0:
                return resources.getString(R.string.tab_overview);
            case 1:
                return resources.getString(R.string.tab_reviews);
            case 2:
                return resources.getString(R.string.tab_cast);
            case 3:
                return resources.getString(R.string.tab_top_rated);
            default:
                return resources.getString(R.string.tab_home);
        }
    }

    @Override
    public int getCount() {
        return 3;
    }

    public int getMovieId() {
        return movieId;
    }

    public void setMovieId(int movieId) {
        this.movieId = movieId;
        bundle.putInt(MovieDetailActivity.EXTRA_MOVIE_ID, movieId);
    }
}
