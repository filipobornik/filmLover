package com.android.obornik.filip.filmlover.data;

import com.android.obornik.filip.filmlover.data.model.castAndCrew.Cast;
import com.android.obornik.filip.filmlover.data.model.castAndCrew.Crew;
import com.android.obornik.filip.filmlover.data.model.castAndCrew.PersonDetail;
import com.android.obornik.filip.filmlover.data.model.credits.ActorsMovieCredit;
import com.android.obornik.filip.filmlover.data.model.credits.CrewMovieCredit;
import com.android.obornik.filip.filmlover.data.model.movie.Movie;
import com.android.obornik.filip.filmlover.data.model.movie.MovieDetail;
import com.android.obornik.filip.filmlover.data.model.review.ReviewsList;
import com.android.obornik.filip.filmlover.data.remote.TMDBService;
import com.android.obornik.filip.filmlover.utils.Constants;
import com.android.obornik.filip.filmlover.utils.schedulers.SchedulersProvider;
import io.reactivex.Flowable;

import javax.inject.Inject;
import javax.inject.Singleton;
import java.util.List;

@Singleton
public class DataManager {

    private TMDBService tmdbService;
    private SchedulersProvider schedulersProvider;
    private String language = Constants.LANGUAGE;
    private int pages = 1;

    @Inject
    public DataManager(TMDBService tmdbService, SchedulersProvider schedulersProvider) {
        this.tmdbService = tmdbService;
        this.schedulersProvider = schedulersProvider;
        language = Constants.LANGUAGE;
    }

    public Flowable<List<Movie>> getMovies(@Constants.MovieType int movieType) {
        switch (movieType) {
            case Constants.POPULAR_MOVIES:
                return getPopularMovies();
            case Constants.TOP_RATED_MOVIES:
                return getTopRatedMovies();
            case Constants.NOW_PLAYING_MOVIES:
                return getNowPlayingMovies();
            case Constants.UPCOMING_MOVIES:
                return getUpcomingMovies();
            default:
                return getPopularMovies();
        }
    }

    public Flowable<MovieDetail> getMovieDetails(int movieId) {
        return tmdbService.getMovieDetails(movieId, language)
                .subscribeOn(schedulersProvider.io())
                .observeOn(schedulersProvider.ui());
    }

    private Flowable<List<Movie>> getPopularMovies() {
        return tmdbService
                .getPopularMovies(language, pages)
                .subscribeOn(schedulersProvider.io())
                .observeOn(schedulersProvider.ui())
                .flatMap(listOfMovies -> Flowable.just(listOfMovies.getMovies()));
    }

    private Flowable<List<Movie>> getTopRatedMovies() {
        return tmdbService
                .getTopRatedMovies(language, pages)
                .subscribeOn(schedulersProvider.io())
                .observeOn(schedulersProvider.ui())
                .flatMap(listOfMovies -> Flowable.just(listOfMovies.getMovies()));
    }

    private Flowable<List<Movie>> getUpcomingMovies() {
        return tmdbService
                .getUpcomingMovies(language, pages)
                .subscribeOn(schedulersProvider.io())
                .observeOn(schedulersProvider.ui())
                .flatMap(listOfMovies -> Flowable.just(listOfMovies.getMovies()));
    }

    private Flowable<List<Movie>> getNowPlayingMovies() {
        return tmdbService
                .getNowPlayingMovies(language, pages)
                .subscribeOn(schedulersProvider.io())
                .observeOn(schedulersProvider.ui())
                .flatMap(listOfMovies -> Flowable.just(listOfMovies.getMovies()));
    }

    public Flowable<ReviewsList> getReviews(int movieId, int pages) {
        return tmdbService.getMovieReviews(movieId, language, pages)
                .subscribeOn(schedulersProvider.io())
                .observeOn(schedulersProvider.ui());
    }

    public Flowable<List<Crew>> getCrew(int movieId, int pages) {
        return tmdbService.getCastAndCrew(movieId, language, pages)
                .subscribeOn(schedulersProvider.io())
                .observeOn(schedulersProvider.ui())
                .flatMap(castAndCrew -> Flowable.just(castAndCrew.getCrew()));
    }

    public Flowable<List<Cast>> getCast(int movieId, int pages) {
        return tmdbService.getCastAndCrew(movieId, language, pages)
                .subscribeOn(schedulersProvider.io())
                .observeOn(schedulersProvider.ui())
                .flatMap(castAndCrew -> Flowable.just(castAndCrew.getCast()));
    }

    public Flowable<PersonDetail> getPersonDetail(int personId) {
        return tmdbService.getPersonDetail(personId, language)
                .subscribeOn(schedulersProvider.io())
                .observeOn(schedulersProvider.ui());
    }

    public Flowable<List<ActorsMovieCredit>> getActorMovieCredit(int personId) {
        return tmdbService.getActorCrewMovieCredit(personId, language)
                .subscribeOn(schedulersProvider.io())
                .observeOn(schedulersProvider.ui())
                .flatMap(actorCrewMovieCredit -> Flowable.just(actorCrewMovieCredit.getActorsMovieCredits()));
    }

    public Flowable<List<CrewMovieCredit>> getCrewMovieCredit(int personId) {
        return tmdbService.getActorCrewMovieCredit(personId, language)
                .subscribeOn(schedulersProvider.io())
                .observeOn(schedulersProvider.ui())
                .flatMap(actorCrewMovieCredit -> Flowable.just(actorCrewMovieCredit.getCrewMovieCredits()));
    }
}
