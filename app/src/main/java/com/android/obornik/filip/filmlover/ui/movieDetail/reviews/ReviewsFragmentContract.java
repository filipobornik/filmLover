package com.android.obornik.filip.filmlover.ui.movieDetail.reviews;

import com.android.obornik.filip.filmlover.common.presenter.BasePresenter;
import com.android.obornik.filip.filmlover.common.view.BaseView;
import com.android.obornik.filip.filmlover.data.model.review.Review;

import java.util.List;

public class ReviewsFragmentContract {

    interface View extends BaseView {

        void showReviews(List<Review> reviews);

        void showMoreReviews(List<Review> reviews);

    }

    interface Presenter extends BasePresenter<View> {

        void loadReviews(int movieId);

        void loadMoreReviews(int movieId);

        void onRateBtnClicked();

        void onDeleteBtnClicked();

    }
}
