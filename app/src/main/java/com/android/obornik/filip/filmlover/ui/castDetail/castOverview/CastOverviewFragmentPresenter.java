package com.android.obornik.filip.filmlover.ui.castDetail.castOverview;

import com.android.obornik.filip.filmlover.core.dagger.PerFragment;
import com.android.obornik.filip.filmlover.data.DataManager;

import javax.inject.Inject;

@PerFragment
public class CastOverviewFragmentPresenter implements CastOverviewFragmentContract.Presenter {

    private CastOverviewFragmentContract.View view;
    private DataManager dataManager;

    @Inject
    public CastOverviewFragmentPresenter(DataManager dataManager) {
        this.dataManager = dataManager;
    }

    @Override
    public void attachView(CastOverviewFragmentContract.View view) {
        this.view = view;
    }

    @Override
    public void detachView() {
        this.view = null;
    }

    @Override
    public boolean isViewAttached() {
        return view != null;
    }

    @Override
    public void loadCastDetail(int id) {
        view.showLoading();
        dataManager.getPersonDetail(id)
                .subscribe(personDetail -> {
                    if (isViewAttached()) {
                        view.hideLoading();
                        view.showCastDetails(personDetail);
                    }
                });
    }
}
