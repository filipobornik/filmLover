package com.android.obornik.filip.filmlover.core.dagger.module;

import android.app.Application;
import android.content.Context;
import com.android.obornik.filip.filmlover.core.FilmLoverApplication;
import com.android.obornik.filip.filmlover.core.dagger.ApplicationContext;
import com.android.obornik.filip.filmlover.core.dagger.PerActivity;
import com.android.obornik.filip.filmlover.data.remote.NetModule;
import com.android.obornik.filip.filmlover.ui.castDetail.CastDetailActivity;
import com.android.obornik.filip.filmlover.ui.castDetail.CastDetailActivityModule;
import com.android.obornik.filip.filmlover.ui.main.MainActivity;
import com.android.obornik.filip.filmlover.ui.main.MainActivityModule;
import com.android.obornik.filip.filmlover.ui.movieDetail.MovieDetailActivity;
import com.android.obornik.filip.filmlover.ui.movieDetail.MovieDetailActivityModule;
import dagger.Binds;
import dagger.Module;
import dagger.android.AndroidInjectionModule;
import dagger.android.ContributesAndroidInjector;

import javax.inject.Singleton;

/**
 * Created by Filip on 11/18/2017.
 */

@Module(includes = {AndroidInjectionModule.class, NetModule.class})
public abstract class AppModule {

    @Binds
    @ApplicationContext
    @Singleton
    abstract Context provideContext(FilmLoverApplication application);

    @Binds
    @Singleton
    abstract Application provideApplication(FilmLoverApplication application);

    @PerActivity
    @ContributesAndroidInjector(modules = MainActivityModule.class)
    abstract MainActivity mainActivityInjector();

    @PerActivity
    @ContributesAndroidInjector(modules = MovieDetailActivityModule.class)
    abstract MovieDetailActivity movieDetailActivity();

    @PerActivity
    @ContributesAndroidInjector(modules = CastDetailActivityModule.class)
    abstract CastDetailActivity castDetailActivity();


}
