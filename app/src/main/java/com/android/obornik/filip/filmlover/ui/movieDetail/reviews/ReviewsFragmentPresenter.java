package com.android.obornik.filip.filmlover.ui.movieDetail.reviews;

import android.util.Log;
import com.android.obornik.filip.filmlover.core.dagger.PerFragment;
import com.android.obornik.filip.filmlover.data.DataManager;
import com.android.obornik.filip.filmlover.utils.schedulers.SchedulersProvider;

import javax.inject.Inject;

/**
 * Created by Filip on 1/20/2018.
 */

@PerFragment
public class ReviewsFragmentPresenter implements ReviewsFragmentContract.Presenter {

    private static final String LOG_TAG = ReviewsFragmentPresenter.class.getSimpleName();

    private ReviewsFragmentContract.View view;
    private DataManager dataManager;
    private SchedulersProvider schedulersProvider;

    private int reviewPagesLoaded = 0;

    @Inject
    public ReviewsFragmentPresenter(DataManager dataManager, SchedulersProvider schedulersProvider) {
        this.dataManager = dataManager;
        this.schedulersProvider = schedulersProvider;
    }

    @Override
    public void loadReviews(int movieId) {
        reviewPagesLoaded = 1;
        dataManager.getReviews(movieId, reviewPagesLoaded)
                .subscribeOn(schedulersProvider.io())
                .observeOn(schedulersProvider.ui())
                .subscribe(reviewsList -> {
                    if (isViewAttached()) {
                        Log.d(LOG_TAG, "Reviews were downloaded : " + reviewsList.getReviews().size());
                        view.showReviews(reviewsList.getReviews());
                    }
                });
    }

    @Override
    public void loadMoreReviews(int movieId) {
        reviewPagesLoaded++;
        dataManager.getReviews(movieId, reviewPagesLoaded)
                .subscribeOn(schedulersProvider.io())
                .observeOn(schedulersProvider.ui())
                .subscribe(reviewsList -> {
                    if (isViewAttached()) {
                        view.showMoreReviews(reviewsList.getReviews());
                    }
                });
    }

    @Override
    public void onRateBtnClicked() {

    }

    @Override
    public void onDeleteBtnClicked() {

    }

    @Override
    public void attachView(ReviewsFragmentContract.View view) {
        this.view = view;
    }

    @Override
    public void detachView() {
        this.view = null;
    }

    @Override
    public boolean isViewAttached() {
        return view != null;
    }
}
