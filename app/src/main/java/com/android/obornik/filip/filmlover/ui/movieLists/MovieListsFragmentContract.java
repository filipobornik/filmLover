package com.android.obornik.filip.filmlover.ui.movieLists;

import com.android.obornik.filip.filmlover.common.presenter.BasePresenter;
import com.android.obornik.filip.filmlover.common.view.BaseView;

/**
 * Created by Filip on 12/28/2017.
 */

public class MovieListsFragmentContract {

    interface View extends BaseView {

    }

    interface Presenter extends BasePresenter<View> {


    }


}
