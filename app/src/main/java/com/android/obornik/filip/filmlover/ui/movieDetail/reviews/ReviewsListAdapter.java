package com.android.obornik.filip.filmlover.ui.movieDetail.reviews;

import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import butterknife.BindView;
import butterknife.ButterKnife;
import com.android.obornik.filip.filmlover.R;
import com.android.obornik.filip.filmlover.core.dagger.PerFragment;
import com.android.obornik.filip.filmlover.data.model.review.Review;
import com.ms.square.android.expandabletextview.ExpandableTextView;

import javax.inject.Inject;
import java.util.ArrayList;
import java.util.List;

@PerFragment
public class ReviewsListAdapter extends RecyclerView.Adapter<ReviewsListAdapter.MyViewHolder> {

    private static final String LOG_TAG = ReviewsListAdapter.class.getSimpleName();
    private List<Review> reviews;

    @Inject
    public ReviewsListAdapter() {
        reviews = new ArrayList<>();
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.review_item, parent, false);
        return new MyViewHolder(view);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {
        holder.txtViewUser.setText(reviews.get(position).getAuthor());
        holder.expandableTextView.setText(reviews.get(position).getContent());
    }

    @Override
    public int getItemCount() {
        return reviews.size();
    }

    public void updateReviewList(List<Review> reviews) {
        if (reviews != null) {
            Log.d(LOG_TAG, "movies should be showed");
            this.reviews = reviews;
            notifyDataSetChanged();
        }
        Log.d(LOG_TAG, "review list is null");
    }

    public void addMoreReviews(List<Review> reviews) {
        if (reviews != null) {
            this.reviews.addAll(reviews);
            notifyDataSetChanged();
        }
    }


    class MyViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.user) TextView txtViewUser;
        @BindView(R.id.expand_text_view) ExpandableTextView expandableTextView;

        MyViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
}
