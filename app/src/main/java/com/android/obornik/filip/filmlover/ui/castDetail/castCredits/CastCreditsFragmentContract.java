package com.android.obornik.filip.filmlover.ui.castDetail.castCredits;

import android.widget.ListView;
import com.android.obornik.filip.filmlover.common.presenter.BasePresenter;
import com.android.obornik.filip.filmlover.common.view.BaseView;
import com.android.obornik.filip.filmlover.data.model.credits.ActorsMovieCredit;
import com.android.obornik.filip.filmlover.data.model.movie.MovieSharedElement;

import java.util.List;

/**
 * Created by Filip on 2/10/2018.
 */

public class CastCreditsFragmentContract {

    interface View extends BaseView {

        void showCastMovieCredits(List<ActorsMovieCredit> credits);

        void showLoading();

        void hideLoading();

        void showMovieDetailActivity(MovieSharedElement movieSharedElement);

    }


    interface Presenter extends BasePresenter<View> {

        void loadCastMovieCredits(int id);

        void onCreditItemClicked(MovieSharedElement movieSharedElement);

    }

}
