package com.android.obornik.filip.filmlover.data.remote;

import com.android.obornik.filip.filmlover.data.model.castAndCrew.CastAndCrew;
import com.android.obornik.filip.filmlover.data.model.castAndCrew.PersonDetail;
import com.android.obornik.filip.filmlover.data.model.credits.ActorCrewMovieCredit;
import com.android.obornik.filip.filmlover.data.model.movie.ListOfMovies;
import com.android.obornik.filip.filmlover.data.model.movie.MovieDetail;
import com.android.obornik.filip.filmlover.data.model.review.ReviewsList;
import io.reactivex.Flowable;
import retrofit2.http.GET;
import retrofit2.http.Path;
import retrofit2.http.Query;

public interface TMDBService {

    @GET("movie/popular")
    Flowable<ListOfMovies> getPopularMovies(@Query("language") String language, @Query("page") int page);

    @GET("movie/upcoming")
    Flowable<ListOfMovies> getUpcomingMovies(@Query("language") String language, @Query("page") int page);

    @GET("movie/top_rated")
    Flowable<ListOfMovies> getTopRatedMovies(@Query("language") String language, @Query("page") int page);

    @GET("movie/now_playing")
    Flowable<ListOfMovies> getNowPlayingMovies(@Query("language") String language, @Query("page") int page);

    @GET("movie/{movie_id}")
    Flowable<MovieDetail> getMovieDetails(@Path("movie_id") int movieId, @Query("language") String language);

    @GET("movie/{movie_id}/reviews")
    Flowable<ReviewsList> getMovieReviews(@Path("movie_id") int movieId, @Query("language") String language, @Query("page") int page);

    @GET("movie/{movie_id}/credits")
    Flowable<CastAndCrew> getCastAndCrew(@Path("movie_id") int movieId, @Query("language") String language, @Query("page") int page);

    @GET("person/{person_id}")
    Flowable<PersonDetail> getPersonDetail(@Path("person_id") int personId, @Query("language") String language);

    @GET("person/{person_id}/movie_credits")
    Flowable<ActorCrewMovieCredit> getActorCrewMovieCredit(@Path("person_id") int personId, @Query("language") String language);
}

