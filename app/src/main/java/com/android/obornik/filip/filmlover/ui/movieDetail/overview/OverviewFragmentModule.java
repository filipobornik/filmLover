package com.android.obornik.filip.filmlover.ui.movieDetail.overview;

import android.support.v4.app.Fragment;
import com.android.obornik.filip.filmlover.common.base.BaseFragmentModule;
import com.android.obornik.filip.filmlover.core.dagger.PerFragment;
import dagger.Binds;
import dagger.Module;
import dagger.Provides;

import javax.inject.Named;

/**
 * Created by Filip on 1/15/2018.
 */

@Module(includes = BaseFragmentModule.class)
public abstract class OverviewFragmentModule {

    @Binds
    @Named(BaseFragmentModule.FRAGMENT)
    @PerFragment
    abstract Fragment provideFragment(OverviewFragment overviewFragment);

    @Binds
    @Named(BaseFragmentModule.FRAGMENT)
    @PerFragment
    abstract OverviewFragmentContract.View provideOverviewFragmentAsView(OverviewFragment overviewFragment);

    @Binds
    @PerFragment
    abstract OverviewFragmentContract.Presenter providePresenter(OverviewFragmentPresenter overviewFragmentPresenter);

}
