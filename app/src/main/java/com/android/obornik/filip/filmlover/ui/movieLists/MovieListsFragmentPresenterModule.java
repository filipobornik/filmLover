package com.android.obornik.filip.filmlover.ui.movieLists;

import com.android.obornik.filip.filmlover.core.dagger.PerFragment;
import dagger.Binds;
import dagger.Module;
import dagger.Provides;

/**
 * Created by Filip on 12/28/2017.
 */

@Module
public abstract class MovieListsFragmentPresenterModule {

    @Binds
    @PerFragment
    abstract MovieListsFragmentContract.Presenter movieListsFragmentPresenter(MovieListsFragmentPresenter presenter);

}
