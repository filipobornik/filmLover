package com.android.obornik.filip.filmlover.core.dagger.component;

import com.android.obornik.filip.filmlover.core.FilmLoverApplication;
import com.android.obornik.filip.filmlover.core.dagger.module.AppModule;
import dagger.Component;
import dagger.android.AndroidInjector;

import javax.inject.Singleton;

/**
 * Created by Filip on 11/27/2017.
 */

@Singleton
@Component(modules = AppModule.class)
public
interface AppComponent extends AndroidInjector<FilmLoverApplication> {

    @Component.Builder
    abstract class Builder extends AndroidInjector.Builder<FilmLoverApplication> {}

}
