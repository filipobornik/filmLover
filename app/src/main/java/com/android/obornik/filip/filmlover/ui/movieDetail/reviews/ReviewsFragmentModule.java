package com.android.obornik.filip.filmlover.ui.movieDetail.reviews;

import android.content.Context;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import com.android.obornik.filip.filmlover.common.base.BaseFragmentModule;
import com.android.obornik.filip.filmlover.core.dagger.ActivityContext;
import com.android.obornik.filip.filmlover.core.dagger.PerFragment;
import dagger.Binds;
import dagger.Module;
import dagger.Provides;

import javax.inject.Named;

/**
 * Created by Filip on 1/20/2018.
 */

@Module(includes = BaseFragmentModule.class)
public abstract class ReviewsFragmentModule {

    @Binds
    @PerFragment
    @Named(BaseFragmentModule.FRAGMENT)
    abstract Fragment provideFragment(ReviewsFragment reviewsFragment);

    @Binds
    @PerFragment
    @Named(BaseFragmentModule.FRAGMENT)
    abstract ReviewsFragmentContract.View provideFramgentAsView(ReviewsFragment reviewsFragment);

    @Binds
    @PerFragment
    abstract ReviewsFragmentContract.Presenter providePresenter(ReviewsFragmentPresenter presenter);

}
