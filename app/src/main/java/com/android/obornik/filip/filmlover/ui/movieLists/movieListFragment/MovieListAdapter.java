package com.android.obornik.filip.filmlover.ui.movieLists.movieListFragment;

import android.content.Context;
import android.content.res.Resources;
import android.os.Build;
import android.support.v4.view.ViewCompat;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import butterknife.BindView;
import butterknife.ButterKnife;
import com.android.obornik.filip.filmlover.R;
import com.android.obornik.filip.filmlover.core.dagger.ActivityContext;
import com.android.obornik.filip.filmlover.core.dagger.PerChildFragment;
import com.android.obornik.filip.filmlover.data.model.movie.Movie;
import com.android.obornik.filip.filmlover.data.model.movie.MovieSharedElement;
import com.android.obornik.filip.filmlover.utils.Constants;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.resource.drawable.DrawableTransitionOptions;
import com.bumptech.glide.request.RequestOptions;
import io.reactivex.Observable;
import io.reactivex.subjects.PublishSubject;

import static com.bumptech.glide.request.RequestOptions.centerCropTransform;

import javax.inject.Inject;
import java.util.ArrayList;
import java.util.List;

@PerChildFragment
public class MovieListAdapter extends RecyclerView.Adapter<MovieListAdapter.MyViewHolder> {

    private static final String LOG_TAG = MovieListAdapter.class.getSimpleName();
    private List<Movie> movies;
    private Context context;
    private PublishSubject<MovieSharedElement> itemOnClickObservable;

    @Inject Resources resources;

    @Inject
    public MovieListAdapter(@ActivityContext Context context) {
        this.movies = new ArrayList<>();
        this.context = context;
        itemOnClickObservable = PublishSubject.create();
        Log.d(LOG_TAG, "adapter created");
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_movie_list, parent, false);
        return new MyViewHolder(view);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {
        Movie movie = movies.get(position);
        Glide.with(context)
                .load(Constants.BASE_POSTER_URL + movie.getPosterPath())
                .apply(new RequestOptions().override(resources.getDisplayMetrics().widthPixels/3, resources.getDisplayMetrics().widthPixels/2))
                .apply(centerCropTransform())
                .transition(DrawableTransitionOptions.withCrossFade())
                .into(holder.imgMoviePoster);

        String movieTitle = movie.getTitle();
        if (movieTitle.length() > 15) {
            movieTitle = movieTitle.substring(0, 11) + "...";
        }
        holder.txtViewMovieTitle.setText(movieTitle);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            ViewCompat.setTransitionName(holder.imgMoviePoster, movie.getPosterPath());
            ViewCompat.setTransitionName(holder.txtViewMovieTitle, movie.getTitle());
        }

        holder.imgMoviePoster.setOnClickListener(view -> movieSelected(movies.get(position).getId(), view, holder.txtViewMovieTitle));
    }



    public Observable<MovieSharedElement> getItemOnClickObservable() {
        return itemOnClickObservable;
    }

    @Override
    public int getItemCount() {
        return movies.size();
    }

    public void updateMovies(List<Movie> movies) {
        this.movies = movies;
        notifyDataSetChanged();
    }

    class MyViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.itemMoviePoster) ImageView imgMoviePoster;
        @BindView(R.id.movieTitle) TextView txtViewMovieTitle;

        public MyViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }

    public void movieSelected(int movieId, View viewPosterImage, View viewMovieTitle) {
        itemOnClickObservable.onNext(new MovieSharedElement(movieId, viewPosterImage, viewMovieTitle));
    }
}
