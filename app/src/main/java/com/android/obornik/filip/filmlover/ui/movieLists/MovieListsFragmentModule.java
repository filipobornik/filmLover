package com.android.obornik.filip.filmlover.ui.movieLists;

import android.support.v4.app.Fragment;
import com.android.obornik.filip.filmlover.common.base.BaseFragmentModule;
import com.android.obornik.filip.filmlover.core.dagger.PerChildFragment;
import com.android.obornik.filip.filmlover.core.dagger.PerFragment;
import com.android.obornik.filip.filmlover.ui.movieLists.movieListFragment.MovieListFragment;
import com.android.obornik.filip.filmlover.ui.movieLists.movieListFragment.MovieListFragmentModule;
import dagger.Binds;
import dagger.Module;
import dagger.android.ContributesAndroidInjector;

import javax.inject.Named;

/**
 * Created by Filip on 12/26/2017.
 */

@Module(includes = {BaseFragmentModule.class, MovieListsFragmentPresenterModule.class})
public abstract class MovieListsFragmentModule {

    @PerChildFragment
    @ContributesAndroidInjector(modules = MovieListFragmentModule.class)
    abstract MovieListFragment movieListFragment();

    @Binds
    @Named(BaseFragmentModule.FRAGMENT)
    @PerFragment
    abstract Fragment movieListsFragment(MovieListsFragment fragment);

    @Binds
    @PerFragment
    abstract MovieListsFragmentContract.View movieListsFragmentView(MovieListsFragment fragment);

}
