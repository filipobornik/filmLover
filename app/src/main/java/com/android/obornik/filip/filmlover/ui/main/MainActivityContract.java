package com.android.obornik.filip.filmlover.ui.main;

import android.support.v4.app.Fragment;
import com.android.obornik.filip.filmlover.common.presenter.BasePresenter;
import com.android.obornik.filip.filmlover.common.view.BaseView;

/**
 * Created by Filip on 11/15/2017.
 */

public interface MainActivityContract {

    interface View extends BaseView {

    }

    interface Presenter extends BasePresenter<View>{

        void subscribeOnNavigationViewItemSelected();

    }
}
