package com.android.obornik.filip.filmlover.data.model.credits;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class ActorCrewMovieCredit {

    @SerializedName("cast")
    @Expose
    private List<ActorsMovieCredit> actorsMovieCredits = null;
    @SerializedName("crew")
    @Expose
    private List<CrewMovieCredit> crewMovieCredits = null;
    @SerializedName("id")
    @Expose
    private Integer id;

    public List<ActorsMovieCredit> getActorsMovieCredits() {
        return actorsMovieCredits;
    }

    public void setActorsMovieCredits(List<ActorCrewMovieCredit> actorCrewMovieCredits) {
        this.actorsMovieCredits = actorsMovieCredits;
    }

    public List<CrewMovieCredit> getCrewMovieCredits() {
        return crewMovieCredits;
    }

    public void setCrewMovieCredits(List<CrewMovieCredit> crewMovieCredits) {
        this.crewMovieCredits = crewMovieCredits;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }
}
