package com.android.obornik.filip.filmlover.ui.main;

import android.content.Context;
import android.support.v7.app.AppCompatActivity;
import com.android.obornik.filip.filmlover.common.base.BaseActivityModule;
import com.android.obornik.filip.filmlover.core.dagger.ActivityContext;
import com.android.obornik.filip.filmlover.core.dagger.PerActivity;
import com.android.obornik.filip.filmlover.core.dagger.PerFragment;
import com.android.obornik.filip.filmlover.ui.movieLists.MovieListsFragmentModule;
import com.android.obornik.filip.filmlover.ui.movieLists.MovieListsFragment;
import dagger.Binds;
import dagger.Module;
import dagger.android.ContributesAndroidInjector;

/**
 * Created by Filip on 11/27/2017.
 */

@Module(includes = {BaseActivityModule.class, MainActivityPresenterModule.class})
public abstract class MainActivityModule {

    @PerFragment
    @ContributesAndroidInjector(modules = MovieListsFragmentModule.class)
    abstract MovieListsFragment popularMoviesListFragment();

    @Binds
    @PerActivity
    abstract MainActivityContract.View provideMainActivityAsView(MainActivity mainActivity);

    @Binds
    @PerActivity
    abstract AppCompatActivity provideMainActivity(MainActivity mainActivity);

    @Binds
    @ActivityContext
    @PerActivity
    abstract Context provideMainActivityContext(MainActivity mainActivity);

}
