package com.android.obornik.filip.filmlover.ui.main;

import com.android.obornik.filip.filmlover.core.dagger.PerActivity;
import com.android.obornik.filip.filmlover.data.DataManager;
import com.android.obornik.filip.filmlover.utils.schedulers.SchedulersProvider;

import javax.inject.Inject;

/**
 * Created by Filip on 11/27/2017.
 */

@PerActivity
public class MainActivityPresenter implements MainActivityContract.Presenter {

    private MainActivityContract.View view;
    private DataManager dataManager;
    private SchedulersProvider schedulersProvider;

    @Inject
    public MainActivityPresenter(MainActivityContract.View view, DataManager dataManager, SchedulersProvider schedulersProvider) {
        this.view = view;
        this.dataManager = dataManager;
        this.schedulersProvider = schedulersProvider;
    }

    @Override
    public void subscribeOnNavigationViewItemSelected() {
        
    }

    @Override
    public void attachView(MainActivityContract.View view) {
        this.view = view;
    }

    @Override
    public void detachView() {
        this.view = null;
    }

    @Override
    public boolean isViewAttached() {
        return view != null;
    }
}
