package com.android.obornik.filip.filmlover.utils;

import android.support.annotation.IntDef;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

public abstract class Constants {

    public static final String BASE_URL_TMDB = "https://api.themoviedb.org/3/";
    public static final String API_KEY = "605786c7d3f247fc5468178fa046d60a";
    public static final String BASE_POSTER_URL = "http://image.tmdb.org/t/p/w185/";
    public static final String BASE_PERSON_IMAGE_URL = "http://image.tmdb.org/t/p/w185";
    public static final String BASE_BACKDROP_URL = "http://image.tmdb.org/t/p/w780/";

    public static final String LANGUAGE = "en-US";

    public static final String MOVIE_TYPE_KEY = "movieType";
    public static final int NOW_PLAYING_MOVIES = 1;
    public static final int UPCOMING_MOVIES = 2;
    public static final int POPULAR_MOVIES = 3;
    public static final int TOP_RATED_MOVIES = 4;

    @IntDef({NOW_PLAYING_MOVIES, UPCOMING_MOVIES, POPULAR_MOVIES, TOP_RATED_MOVIES})
    @Retention(RetentionPolicy.SOURCE)
    public @interface MovieType{}

}
