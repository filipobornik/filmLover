package com.android.obornik.filip.filmlover.ui.movieLists.movieListFragment;

import com.android.obornik.filip.filmlover.data.model.movie.Movie;
import com.android.obornik.filip.filmlover.common.presenter.BasePresenter;
import com.android.obornik.filip.filmlover.common.view.BaseView;
import com.android.obornik.filip.filmlover.data.model.movie.MovieSharedElement;
import com.android.obornik.filip.filmlover.utils.Constants;

import java.util.List;

public interface MovieListFragmentContract {

    interface View extends BaseView {

        void showMovies(List<Movie> movies);

        void showMovieDetailActivity(MovieSharedElement movieSharedElement);

        void showLoadingIndicator();

        void hideLoadingIndicator();

        void showNoNetworkConnectionIndicator();

        @Constants.MovieType int getMovieListType();

    }

    interface Presenter extends BasePresenter<View> {

        void loadMovies();

        void onMovieSelected(Movie movie);

        void onRefresh();

        void onRetryButtonClicked();
    }

}
