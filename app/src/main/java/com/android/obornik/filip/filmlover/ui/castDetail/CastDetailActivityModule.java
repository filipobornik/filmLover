package com.android.obornik.filip.filmlover.ui.castDetail;

import android.content.Context;
import android.support.v7.app.AppCompatActivity;
import butterknife.BindView;
import com.android.obornik.filip.filmlover.common.base.BaseActivityModule;
import com.android.obornik.filip.filmlover.core.dagger.ActivityContext;
import com.android.obornik.filip.filmlover.core.dagger.PerActivity;
import com.android.obornik.filip.filmlover.core.dagger.PerFragment;
import com.android.obornik.filip.filmlover.ui.castDetail.castCredits.CastCreditsFragment;
import com.android.obornik.filip.filmlover.ui.castDetail.castCredits.CastCreditsFragmentModule;
import com.android.obornik.filip.filmlover.ui.castDetail.castOverview.CastOverviewFragment;
import com.android.obornik.filip.filmlover.ui.castDetail.castOverview.CastOverviewFragmentModule;
import dagger.Binds;
import dagger.Module;
import dagger.android.ContributesAndroidInjector;

import javax.inject.Named;

@Module(includes = BaseActivityModule.class)
public abstract class CastDetailActivityModule {

    @PerFragment
    @ContributesAndroidInjector(modules = CastOverviewFragmentModule.class)
    abstract CastOverviewFragment castOverviewFragment();

    @PerFragment
    @ContributesAndroidInjector(modules = CastCreditsFragmentModule.class)
    abstract CastCreditsFragment castCreditsFragment();

    @Binds
    @PerActivity
    abstract CastDetailActivityContract.View provideActivityAsView(CastDetailActivity castDetailActivity);

    @Binds
    @PerActivity
    abstract AppCompatActivity provideActivity(CastDetailActivity castDetailActivity);

    @ActivityContext
    @Binds
    @PerActivity
    abstract Context context(CastDetailActivity castDetailActivity);

    @Binds
    @PerActivity
    abstract CastDetailActivityContract.Presenter providePresenter(CastDetailActivityPresenter castDetailActivityPresenter);


}
