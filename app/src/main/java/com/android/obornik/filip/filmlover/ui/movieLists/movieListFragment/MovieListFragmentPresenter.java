package com.android.obornik.filip.filmlover.ui.movieLists.movieListFragment;

import android.content.Intent;
import com.android.obornik.filip.filmlover.core.dagger.PerChildFragment;
import com.android.obornik.filip.filmlover.data.DataManager;
import com.android.obornik.filip.filmlover.data.model.movie.Movie;
import com.android.obornik.filip.filmlover.data.remote.NetworkMonitor;
import com.android.obornik.filip.filmlover.utils.schedulers.SchedulersProvider;
import io.reactivex.disposables.CompositeDisposable;

import javax.inject.Inject;

@PerChildFragment
public class MovieListFragmentPresenter implements MovieListFragmentContract.Presenter {

    private CompositeDisposable compositeDisposable;
    private MovieListFragmentContract.View view;
    private DataManager dataManager;
    private SchedulersProvider schedulersProvider;
    private NetworkMonitor networkMonitor;

    @Inject
    public MovieListFragmentPresenter(CompositeDisposable compositeDisposable, DataManager dataManager,
                                      SchedulersProvider schedulersProvider, NetworkMonitor networkMonitor) {
        this.dataManager = dataManager;
        this.schedulersProvider = schedulersProvider;
        this.compositeDisposable = compositeDisposable;
        this.networkMonitor = networkMonitor;
    }

    @Override
    public void attachView(MovieListFragmentContract.View view) {
        this.view = view;
    }

    @Override
    public void detachView() {
        this.view = null;
    }

    @Override
    public boolean isViewAttached() {
        return view != null;
    }

    @Override
    public void loadMovies() {
        if (!networkMonitor.isConnected()){
            view.showNoNetworkConnectionIndicator();
        }
        view.showLoadingIndicator();
        compositeDisposable.add(dataManager
                .getMovies(view.getMovieListType())
                .subscribeOn(schedulersProvider.io())
                .observeOn(schedulersProvider.ui())
                .subscribe(
                        movies -> {
                            if (isViewAttached()) {
                                view.hideLoadingIndicator();
                                view.showMovies(movies);
                            }
                        },
                        throwable -> {
                                view.hideLoadingIndicator();
                                view.showError(throwable.getMessage());
                        }));
    }

    @Override
    public void onMovieSelected(Movie movie) {
        Intent intent = new Intent();
    }

    @Override
    public void onRefresh() {
        loadMovies();
    }

    @Override
    public void onRetryButtonClicked() {
        loadMovies();
    }
}
