package com.android.obornik.filip.filmlover.utils;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

public class Formatter {

    public static String formatDateFromDatabase(String strDate) {
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-mm-dd", Locale.US);
        if (strDate == null || strDate.isEmpty()) {
            return "-";
        }
        try {
            Date date = dateFormat.parse(strDate);
            SimpleDateFormat newDateFormat = new SimpleDateFormat("d. m. yyyy", Locale.US);
            return newDateFormat.format(date);
        } catch (ParseException e) {
            return "";
        }
    }

}
