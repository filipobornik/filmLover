package com.android.obornik.filip.filmlover.ui.movieDetail.reviews;


import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import android.widget.ImageView;
import android.widget.RatingBar;
import butterknife.BindView;
import butterknife.ButterKnife;
import com.android.obornik.filip.filmlover.R;
import com.android.obornik.filip.filmlover.common.base.BaseFragment;
import com.android.obornik.filip.filmlover.data.model.review.Review;
import com.android.obornik.filip.filmlover.ui.movieDetail.MovieDetailActivity;

import javax.inject.Inject;
import java.util.List;

/**
 * A simple {@link Fragment} subclass.
 */
public class ReviewsFragment extends BaseFragment<ReviewsFragmentContract.Presenter> implements ReviewsFragmentContract.View {

    @BindView(R.id.ratingBar) RatingBar ratingBar;
    @BindView(R.id.userAvatar) ImageView circleImageView;
    @BindView(R.id.reviewsRecyclerView) RecyclerView reviewsRecyclerView;

    @Inject ReviewsListAdapter reviewsListAdapter;

    private int movieId;

    public ReviewsFragment() {}

    @Override
    public void showReviews(List<Review> reviews) {
        reviewsListAdapter.updateReviewList(reviews);
    }

    @Override
    public void showMoreReviews(List<Review> reviews) {
        reviewsListAdapter.addMoreReviews(reviews);
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_reviews, container, false);
        ButterKnife.bind(this, view);
        if (getArguments() != null) {
            movieId = getArguments().getInt(MovieDetailActivity.EXTRA_MOVIE_ID);
        }
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this.getContext(), LinearLayoutManager.VERTICAL, false);
        reviewsRecyclerView.setLayoutManager(linearLayoutManager);
        reviewsRecyclerView.setNestedScrollingEnabled(false);
//        reviewsRecyclerView.addItemDecoration(new DividerItemDecoration(reviewsRecyclerView.getContext(), linearLayoutManager.getOrientation()));
        reviewsRecyclerView.setAdapter(reviewsListAdapter);
        return view;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        presenter.attachView(this);
        presenter.loadReviews(movieId);
        super.onViewCreated(view, savedInstanceState);
    }
}
