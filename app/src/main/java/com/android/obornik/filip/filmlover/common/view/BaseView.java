package com.android.obornik.filip.filmlover.common.view;

import android.support.annotation.IdRes;
import android.support.annotation.StringRes;
import android.support.v4.app.Fragment;
import com.android.obornik.filip.filmlover.common.presenter.BasePresenter;

/**
 * Created by Filip on 11/18/2017.
 */

public interface BaseView {

    void showMessage(String message);

    void showMessage(@StringRes int messageResourceId);

    void showError(String error);

    void showError(@StringRes int messageResourceId);

    void addFragment(@IdRes int containerViewId, Fragment fragment);
}
