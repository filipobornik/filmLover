package com.android.obornik.filip.filmlover.ui.castDetail;

import com.android.obornik.filip.filmlover.core.dagger.PerActivity;
import com.android.obornik.filip.filmlover.data.DataManager;

import javax.inject.Inject;

@PerActivity
public class CastDetailActivityPresenter implements CastDetailActivityContract.Presenter {

    private CastDetailActivityContract.View view;
    private DataManager dataManager;

    @Inject
    public CastDetailActivityPresenter(DataManager dataManager) {
        this.dataManager = dataManager;
    }

    @Override
    public void attachView(CastDetailActivityContract.View view) {
        this.view = view;
    }

    @Override
    public void detachView() {
        this.view = null;
    }

    @Override
    public boolean isViewAttached() {
        return view != null;
    }

    @Override
    public void loadCastDetails(int castId) {
        dataManager.getPersonDetail(castId)
                .subscribe(personDetail -> {
                    if (isViewAttached()) {
                        view.showCastDetail(personDetail);
                    }
                });
    }
}
