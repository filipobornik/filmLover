package com.android.obornik.filip.filmlover.ui.movieLists;


import android.os.Bundle;
import android.support.annotation.IntDef;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import butterknife.BindView;
import butterknife.ButterKnife;
import com.android.obornik.filip.filmlover.R;
import com.android.obornik.filip.filmlover.common.base.BaseFragment;
import com.android.obornik.filip.filmlover.utils.Constants;

import javax.inject.Inject;

/**
 * A simple {@link Fragment} subclass.
 */
public class MovieListsFragment extends BaseFragment<MovieListsFragmentContract.Presenter> implements MovieListsFragmentContract.View {

    private static final String LOG_TAG = MovieListsFragment.class.getSimpleName();
    private final String CURRENT_ITEM = "currentItem";




    @BindView(R.id.pager) ViewPager viewPager;
    @BindView(R.id.tabs) TabLayout tabLayout;
    @Inject ViewPagerAdapter viewPagerAdapter;

    public MovieListsFragment() {
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_movie_lists, container, false);
        ButterKnife.bind(this, view);

        Log.d(LOG_TAG, "created");
       // setRetainInstance(true);
        viewPager.setAdapter(viewPagerAdapter);
        tabLayout.setupWithViewPager(viewPager);


        return view;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
    }
//
//    @Override
//    public void onViewStateRestored(@Nullable Bundle savedInstanceState) {
//        super.onViewStateRestored(savedInstanceState);
//
//        if (savedInstanceState != null) {
//            if (savedInstanceState.getInt(CURRENT_ITEM, -1) != -1) {
//                Log.d(LOG_TAG, "Current position should be: " + savedInstanceState.getInt(CURRENT_ITEM));
//                viewPager.setCurrentItem(savedInstanceState.getInt(CURRENT_ITEM));
//            }
//        }
//    }
//
//
//    @Override
//    public void onSaveInstanceState(@NonNull Bundle outState) {
//        outState.putInt(CURRENT_ITEM, viewPager.getCurrentItem());
//        Log.d(LOG_TAG, "Current position IS: " + viewPager.getCurrentItem());
//        super.onSaveInstanceState(outState);
//    }


}
