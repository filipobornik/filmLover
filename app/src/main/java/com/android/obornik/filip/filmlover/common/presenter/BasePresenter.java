package com.android.obornik.filip.filmlover.common.presenter;

import com.android.obornik.filip.filmlover.common.view.BaseView;

/**
 * Created by Filip on 11/18/2017.
 */

public interface BasePresenter<V extends BaseView> {
    void attachView(V view);
    void detachView();
    boolean isViewAttached();
}