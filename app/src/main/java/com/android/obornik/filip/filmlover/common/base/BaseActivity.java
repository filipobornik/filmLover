package com.android.obornik.filip.filmlover.common.base;

import android.os.Bundle;
import android.support.annotation.IdRes;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.AppCompatActivity;
import android.widget.Toast;
import butterknife.ButterKnife;
import com.android.obornik.filip.filmlover.common.presenter.BasePresenter;
import com.android.obornik.filip.filmlover.common.view.BaseView;
import dagger.android.*;
import dagger.android.support.HasSupportFragmentInjector;

import javax.inject.Inject;
import javax.inject.Named;

/**
 * Created by Filip on 11/30/2017.
 */

public class BaseActivity<T extends BasePresenter> extends AppCompatActivity implements BaseView{

    @Named(BaseActivityModule.ACTIVITY_FRAGMENT_MANAGER)
    @Inject protected FragmentManager fragmentManager;
    @Inject protected T presenter;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        AndroidInjection.inject(this);
        super.onCreate(savedInstanceState);
    }

    @Override
    public final void addFragment(@IdRes int containerViewId, Fragment fragment) {
        fragmentManager.beginTransaction()
                .add(containerViewId, fragment)
                .commit();
    }

    @Override
    public void showMessage(String message) {
        Toast.makeText(this, message, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void showMessage(int messageResourceId) {
        Toast.makeText(this, getResources().getText(messageResourceId), Toast.LENGTH_SHORT).show();
    }

    @Override
    public void showError(String error) {

    }

    @Override
    public void showError(int messageResourceId) {

    }

}
