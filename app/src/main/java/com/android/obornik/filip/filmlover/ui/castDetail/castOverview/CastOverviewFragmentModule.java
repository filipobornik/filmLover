package com.android.obornik.filip.filmlover.ui.castDetail.castOverview;

import android.support.v4.app.Fragment;
import com.android.obornik.filip.filmlover.common.base.BaseFragmentModule;
import com.android.obornik.filip.filmlover.core.dagger.PerFragment;
import dagger.Binds;
import dagger.Module;

import javax.inject.Named;

@Module(includes = BaseFragmentModule.class)
public abstract class CastOverviewFragmentModule {

    @Binds
    @PerFragment
    @Named(BaseFragmentModule.FRAGMENT)
    abstract CastOverviewFragmentContract.View provideFragmentAdView(CastOverviewFragment castOverviewFragment);

    @Binds
    @PerFragment
    @Named(BaseFragmentModule.FRAGMENT)
    abstract Fragment provideFragment(CastOverviewFragment castOverviewFragment);

    @Binds
    @PerFragment
    abstract CastOverviewFragmentContract.Presenter presenter(CastOverviewFragmentPresenter presenter);
}
