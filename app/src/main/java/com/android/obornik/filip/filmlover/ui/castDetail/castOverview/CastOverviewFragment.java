package com.android.obornik.filip.filmlover.ui.castDetail.castOverview;


import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.CardView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import android.widget.*;
import butterknife.BindView;
import butterknife.ButterKnife;
import com.android.obornik.filip.filmlover.R;
import com.android.obornik.filip.filmlover.common.base.BaseFragment;
import com.android.obornik.filip.filmlover.common.presenter.BasePresenter;
import com.android.obornik.filip.filmlover.common.view.BaseView;
import com.android.obornik.filip.filmlover.core.dagger.PerFragment;
import com.android.obornik.filip.filmlover.data.model.castAndCrew.PersonDetail;
import com.android.obornik.filip.filmlover.ui.castDetail.CastDetailActivity;
import com.android.obornik.filip.filmlover.ui.movieDetail.MovieDetailActivity;
import com.android.obornik.filip.filmlover.utils.Formatter;
import dagger.Binds;

import javax.inject.Inject;

/**
 * A simple {@link Fragment} subclass.
*/

public class CastOverviewFragment extends BaseFragment<CastOverviewFragmentContract.Presenter> implements CastOverviewFragmentContract.View {

    @BindView(R.id.birthday) TextView txtViewBirthday;
    @BindView(R.id.deathday) TextView txtViewDeathday;
    @BindView(R.id.placeOfBirth) TextView txtViewPlaceOfBirth;
    @BindView(R.id.also_known_as_card) CardView cardAlsoKnownAs;
    @BindView(R.id.also_known_as_list) ListView listViewAlsoKnownAs;
    @BindView(R.id.biography) TextView txtViewBiography;
    @BindView(R.id.loading) RelativeLayout loading;
    @BindView(R.id.content) LinearLayout content;

    private int castId = -1;

    public CastOverviewFragment() {
        // Required empty public constructor
    }

    @Override
    public void showCastDetails(PersonDetail castDetail) {
        txtViewBirthday.setText(Formatter.formatDateFromDatabase(castDetail.getBirthday()));
        txtViewDeathday.setText(Formatter.formatDateFromDatabase(castDetail.getDeathday()));
        txtViewPlaceOfBirth.setText(castDetail.getPlaceOfBirth());
        if (!castDetail.getAlsoKnownAs().isEmpty()) {
            cardAlsoKnownAs.setVisibility(View.VISIBLE);
            listViewAlsoKnownAs.setAdapter(new ArrayAdapter<>(this.getActivity(), android.R.layout.simple_list_item_1, android.R.id.text1, castDetail.getAlsoKnownAs()));
        } else {
            cardAlsoKnownAs.setVisibility(View.GONE);
        }
        txtViewBiography.setText(castDetail.getBiography());

    }

    @Override
    public void showLoading() {
        content.setVisibility(View.GONE);
        loading.setVisibility(View.VISIBLE);
    }

    @Override
    public void hideLoading() {
        loading.setVisibility(View.GONE);
        content.setVisibility(View.VISIBLE);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        if (getArguments() != null) {
            castId = getArguments().getInt(CastDetailActivity.EXTRA_CAST_ID);
        }
        return inflater.inflate(R.layout.fragment_cast_overview, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        ButterKnife.bind(this, view);
        presenter.attachView(this);
        presenter.loadCastDetail(castId);
    }
}
