package com.android.obornik.filip.filmlover.ui.movieDetail.cast;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import butterknife.BindView;
import butterknife.ButterKnife;
import com.android.obornik.filip.filmlover.R;
import com.android.obornik.filip.filmlover.core.dagger.ActivityContext;
import com.android.obornik.filip.filmlover.core.dagger.PerFragment;
import com.android.obornik.filip.filmlover.data.model.castAndCrew.Cast;
import com.android.obornik.filip.filmlover.utils.Constants;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.resource.drawable.DrawableTransitionOptions;
import com.bumptech.glide.request.RequestOptions;
import io.reactivex.subjects.PublishSubject;

import javax.inject.Inject;
import java.util.ArrayList;
import java.util.List;

import static com.bumptech.glide.request.RequestOptions.centerCropTransform;

@PerFragment
public class CastListAdapter extends RecyclerView.Adapter<CastListAdapter.MyViewHolder> {

    private PublishSubject<Integer> itemOnClickObservable;
    private List<Cast> cast;
    @ActivityContext private Context context;


    @Inject
    public CastListAdapter(Context context) {
        this.context = context;
        cast = new ArrayList<>();
        itemOnClickObservable = PublishSubject.create();
    }

    @Override
    public CastListAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.actor_item, parent, false);
        return new MyViewHolder(view);
    }

    @Override
    public void onBindViewHolder(CastListAdapter.MyViewHolder holder, int position) {
        holder.txtViewActorsName.setText(cast.get(position).getName());
        holder.txtViewCharacter.setText(cast.get(position).getCharacter());
        Glide.with(context)
                .load(Constants.BASE_POSTER_URL + cast.get(position).getProfilePath())
                .apply(new RequestOptions()
                .placeholder(R.drawable.default_actor_image))
                .apply(centerCropTransform())
                .transition(DrawableTransitionOptions.withCrossFade())
                .into(holder.imgViewActorImage);
    }

    @Override
    public int getItemCount() {
        return cast.size();
    }

    public void updateCastList(List<Cast> cast) {
        this.cast = cast;
        notifyDataSetChanged();
    }

    class MyViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.actors_name) TextView txtViewActorsName;
        @BindView(R.id.actors_image) ImageView imgViewActorImage;
        @BindView(R.id.character) TextView txtViewCharacter;

        public MyViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
            itemView.setOnClickListener(v -> itemOnClickObservable.onNext(cast.get(getAdapterPosition()).getId()));
        }
    }

    public PublishSubject<Integer> getItemOnClickObservable() {
        return itemOnClickObservable;
    }
}
