package com.android.obornik.filip.filmlover.data.remote;

import android.app.Application;
import android.content.Context;
import android.util.Log;
import com.android.obornik.filip.filmlover.core.dagger.ApplicationContext;
import com.android.obornik.filip.filmlover.data.remote.TMDBService;
import com.android.obornik.filip.filmlover.utils.Constants;
import com.android.obornik.filip.filmlover.utils.customExceptions.NoNetworkExceptions;
import com.google.gson.FieldNamingPolicy;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import dagger.Binds;
import dagger.Module;
import dagger.Provides;
import okhttp3.Cache;
import okhttp3.HttpUrl;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;

import javax.inject.Singleton;

/**
 * Created by Filip on 11/20/2017.
 */

@Module
public class NetModule {

    private static final String LOG_TAG = NetModule.class.getSimpleName();

    @Provides
    @Singleton
    Cache provideHttpCache(Application application) {
        int cacheSize = 10 * 1024 * 1024;
        Cache cache = new Cache(application.getCacheDir(), cacheSize);
        return cache;
    }

    @Provides
    @Singleton
    Gson provideGson() {
        GsonBuilder builder = new GsonBuilder();
        builder.setFieldNamingPolicy(FieldNamingPolicy.LOWER_CASE_WITH_UNDERSCORES);
        return builder.create();
    }

    @Provides
    @Singleton
    OkHttpClient provideOkHttpClient(Cache cache) {
        OkHttpClient.Builder clientBuilder = new OkHttpClient.Builder();
//
//        clientBuilder.addInterceptor(chain -> {
//            if (networkMonitor.isConnected()) {
//                return chain.proceed(chain.request());
//            } else {
//                throw new NoNetworkExceptions();
//            }
//        });

        clientBuilder.cache(cache);
        clientBuilder.addInterceptor(chain -> {
            Request originalRequest = chain.request();
            HttpUrl httpUrl = originalRequest.url();

            HttpUrl newHttpUrl = httpUrl.newBuilder()
                    .addQueryParameter("api_key", Constants.API_KEY)
                    .build();

            Log.d(LOG_TAG, "CLIENT: " + newHttpUrl.toString());

            Request.Builder requestBuilder = originalRequest.newBuilder().url(newHttpUrl);
            Request newRequest = requestBuilder.build();

            return chain.proceed(newRequest);
        });

        return clientBuilder.build();
    }

    @Provides
    @Singleton
    Retrofit provideRetrofit(OkHttpClient okHttpClient, Gson gson) {
        return new Retrofit.Builder()
                .addConverterFactory(GsonConverterFactory.create(gson))
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .baseUrl(Constants.BASE_URL_TMDB)
                .client(okHttpClient)
                .build();
    }

    @Provides
    @Singleton
    TMDBService provideTMDBService(Retrofit retrofit) {
        return retrofit.create(TMDBService.class);
    }
}
