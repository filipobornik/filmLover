package com.android.obornik.filip.filmlover.ui.castDetail.castOverview;

import com.android.obornik.filip.filmlover.common.presenter.BasePresenter;
import com.android.obornik.filip.filmlover.common.view.BaseView;
import com.android.obornik.filip.filmlover.data.model.castAndCrew.PersonDetail;

/**
 * Created by Filip on 2/10/2018.
 */

public class CastOverviewFragmentContract {

    interface View extends BaseView {

        void showCastDetails(PersonDetail personDetail);

        void showLoading();

        void hideLoading();

    }

    interface Presenter extends BasePresenter<View> {

        void loadCastDetail(int id);

    }

}
