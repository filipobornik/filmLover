package com.android.obornik.filip.filmlover.utils.schedulers;

import io.reactivex.Scheduler;
import io.reactivex.annotations.NonNull;

/**
 * Created by Filip on 11/27/2017.
 */

public interface BaseSchedulerProvider {
    @NonNull
    Scheduler computation();

    @NonNull
    Scheduler io();

    @NonNull
    Scheduler ui();
}
