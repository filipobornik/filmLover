package com.android.obornik.filip.filmlover.ui.movieLists;

import android.content.res.Resources;
import android.os.Bundle;
import android.support.v4.app.*;
import com.android.obornik.filip.filmlover.R;
import com.android.obornik.filip.filmlover.common.base.BaseFragmentModule;
import com.android.obornik.filip.filmlover.core.dagger.PerFragment;
import com.android.obornik.filip.filmlover.ui.movieLists.movieListFragment.MovieListFragment;
import com.android.obornik.filip.filmlover.utils.Constants;

import javax.inject.Inject;
import javax.inject.Named;

/**
 * Created by Filip on 9/23/2017.
 */

@PerFragment
public class ViewPagerAdapter extends FragmentStatePagerAdapter {
    private Resources resources;

    @Inject
    ViewPagerAdapter(@Named(BaseFragmentModule.CHILD_FRAGMENT_MANAGER) FragmentManager fragmentManager, Resources resources) {
        super(fragmentManager);
        this.resources = resources;
    }

    @Override
    public Fragment getItem(int position) {
        Bundle bundle = new Bundle();
        MovieListFragment movieListFragment;
        switch (position) {
            case 1:
                movieListFragment = new MovieListFragment();
                bundle.putInt(Constants.MOVIE_TYPE_KEY, Constants.NOW_PLAYING_MOVIES);
                movieListFragment.setArguments(bundle);
                return movieListFragment;
            case 2:
                movieListFragment = new MovieListFragment();
                bundle.putInt(Constants.MOVIE_TYPE_KEY, Constants.UPCOMING_MOVIES);
                movieListFragment.setArguments(bundle);
                return movieListFragment;
            case 3:
                movieListFragment = new MovieListFragment();
                bundle.putInt(Constants.MOVIE_TYPE_KEY, Constants.POPULAR_MOVIES);
                movieListFragment.setArguments(bundle);
                return movieListFragment;
            case 4:
                movieListFragment = new MovieListFragment();
                bundle.putInt(Constants.MOVIE_TYPE_KEY, Constants.TOP_RATED_MOVIES);
                movieListFragment.setArguments(bundle);
                return movieListFragment;
            default:
                movieListFragment = new MovieListFragment();
                bundle.putInt(Constants.MOVIE_TYPE_KEY, Constants.POPULAR_MOVIES);
                movieListFragment.setArguments(bundle);
                return movieListFragment;
        }
    }

    @Override
    public CharSequence getPageTitle(int position) {
        switch (position) {
            case 0:
                return resources.getString(R.string.tab_now_playing);
            case 1:
                return resources.getString(R.string.tab_upcoming);
            case 2:
                return resources.getString(R.string.tab_popular);
            case 3:
                return resources.getString(R.string.tab_top_rated);
            default:
                return resources.getString(R.string.tab_home);
        }
    }

    @Override
    public int getCount() {
        return 4;
    }
}
