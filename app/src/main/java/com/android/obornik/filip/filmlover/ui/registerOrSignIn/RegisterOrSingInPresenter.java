package com.android.obornik.filip.filmlover.ui.registerOrSignIn;

/**
 * Created by Filip on 9/12/2017.
 */

public class RegisterOrSingInPresenter implements RegisterOrSignInContract.Presenter {

    @Override
    public void onSignInBtnClick() {

    }

    @Override
    public void onUseAsGuestBtnClick() {

    }

    @Override
    public void onRegisterTextClick() {

    }
}
