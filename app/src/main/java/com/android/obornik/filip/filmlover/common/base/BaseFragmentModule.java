package com.android.obornik.filip.filmlover.common.base;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import com.android.obornik.filip.filmlover.core.dagger.PerFragment;
import dagger.Module;
import dagger.Provides;

import javax.inject.Named;

/**
 * Created by Filip on 12/1/2017.
 */

@Module
public abstract class BaseFragmentModule {

    public static final String FRAGMENT = "BaseFragmentModule.fragment";

    public static final String CHILD_FRAGMENT_MANAGER = "BaseFragmentModule.childFragmentModule";

    @Provides
    @Named(CHILD_FRAGMENT_MANAGER)
    @PerFragment
    static FragmentManager childFragmentManager(@Named(FRAGMENT) Fragment fragment) {
        return fragment.getChildFragmentManager();
    }

}
