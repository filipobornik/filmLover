package com.android.obornik.filip.filmlover.ui.castDetail;

import com.android.obornik.filip.filmlover.common.presenter.BasePresenter;
import com.android.obornik.filip.filmlover.common.view.BaseView;
import com.android.obornik.filip.filmlover.data.model.castAndCrew.Cast;
import com.android.obornik.filip.filmlover.data.model.castAndCrew.PersonDetail;
import com.android.obornik.filip.filmlover.data.model.credits.ActorsMovieCredit;

import java.util.List;

public class CastDetailActivityContract {

    interface View extends BaseView {

        void showCastDetail(PersonDetail personDetail);

    }

    interface Presenter extends BasePresenter<View> {

        void loadCastDetails(int castId);

    }

}
