package com.android.obornik.filip.filmlover.core.dagger;

import javax.inject.Scope;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

/**
 * Created by Filip on 11/18/2017.
 */

@Scope
@Retention(RetentionPolicy.RUNTIME)
public @interface PerService {
}
