package com.android.obornik.filip.filmlover.ui.castDetail;

import android.content.res.Resources;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import com.android.obornik.filip.filmlover.R;
import com.android.obornik.filip.filmlover.common.base.BaseActivityModule;
import com.android.obornik.filip.filmlover.core.dagger.PerActivity;
import com.android.obornik.filip.filmlover.ui.castDetail.castCredits.CastCreditsFragment;
import com.android.obornik.filip.filmlover.ui.castDetail.castOverview.CastOverviewFragment;

import javax.inject.Inject;
import javax.inject.Named;

@PerActivity
public class ViewPagerAdapter extends FragmentStatePagerAdapter {

    @Inject Resources resources;
    private int id = -1;
    private Bundle bundle;

    @Inject
    public ViewPagerAdapter(@Named(BaseActivityModule.ACTIVITY_FRAGMENT_MANAGER) FragmentManager fm, Resources resources) {
        super(fm);
        this.resources = resources;
        bundle = new Bundle();
    }

    @Override
    public Fragment getItem(int position) {
        switch (position) {
            case 0:
                CastOverviewFragment castOverviewFragment = new CastOverviewFragment();
                castOverviewFragment.setArguments(bundle);
                return castOverviewFragment;
            default:
                CastCreditsFragment castCreditsFragment = new CastCreditsFragment();
                castCreditsFragment.setArguments(bundle);
                return castCreditsFragment;
        }
    }

    @Override
    public CharSequence getPageTitle(int position) {
        switch (position) {
            case 0:
                return resources.getString(R.string.tab_overview);
            case 1:
                return resources.getString(R.string.tab_credits);
            default:
                return resources.getString(R.string.tab_credits);
        }
    }

    @Override
    public int getCount() {
        return 2;
    }

    public void setPersonsId(int id) {
        this.id = id;
        bundle.putInt(CastDetailActivity.EXTRA_CAST_ID, id);
    }
}
