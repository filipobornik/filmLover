package com.android.obornik.filip.filmlover.ui.movieDetail;

import android.graphics.drawable.Drawable;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.CollapsingToolbarLayout;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.ImageView;
import android.widget.TextView;
import butterknife.BindView;
import butterknife.ButterKnife;
import com.android.obornik.filip.filmlover.R;
import com.android.obornik.filip.filmlover.common.base.BaseActivity;
import com.android.obornik.filip.filmlover.data.model.movie.MovieDetail;
import com.android.obornik.filip.filmlover.utils.Constants;
import com.android.obornik.filip.filmlover.utils.Formatter;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.DataSource;
import com.bumptech.glide.load.engine.GlideException;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.RequestOptions;
import com.bumptech.glide.request.target.Target;
import dagger.android.AndroidInjector;
import dagger.android.DispatchingAndroidInjector;
import dagger.android.support.HasSupportFragmentInjector;

import javax.inject.Inject;

public class MovieDetailActivity extends BaseActivity<MovieDetailActivityContract.Presenter> implements MovieDetailActivityContract.View, HasSupportFragmentInjector{

    public static final String EXTRA_MOVIE_POSTER_TRANSITION_NAME = "extraMoviePosterTransitionName";
    public static final String EXTRA_MOVIE_TITLE_TRANSITION_NAME = "extraMovieTitleTransitionName";
    public static final String EXTRA_MOVIE_ID = "extraMovieId";
    public static final String EXTRA_MOVIE_POSTER_DRAWABLE = "extraMoviePosterDrawable";

    private int movieId;

    @BindView(R.id.pager) ViewPager viewPager;
    @BindView(R.id.tabs) TabLayout tabLayout;
    @BindView(R.id.posterImage) ImageView imgViewPoster;
    @BindView(R.id.backdrop) ImageView imgViewBackdrop;
    @BindView(R.id.movieTitle) TextView txtViewMovieTitle;
    @BindView(R.id.basicInfo) TextView txtViewBasicInfo;
    @BindView(R.id.toolbar) Toolbar toolbar;
    @BindView(R.id.collapsing_toolbar_layout) CollapsingToolbarLayout collapsingToolbarLayout;

    @Inject DispatchingAndroidInjector<Fragment> fragmentInjector;
    @Inject MovieDetailActivityContract.Presenter presenter;
    @Inject ViewPagerAdapter viewPagerAdapter;

    @Override
    public void showMovieDetails(MovieDetail movieDetail) {
        Glide.with(this)
                .load(Constants.BASE_POSTER_URL + movieDetail.getPosterPath())
                .apply(new RequestOptions().onlyRetrieveFromCache(true))
                .listener(new RequestListener<Drawable>() {
                    @Override
                    public boolean onLoadFailed(@Nullable GlideException e, Object model, Target<Drawable> target, boolean isFirstResource) {
                        supportStartPostponedEnterTransition();
                        return false;
                    }

                    @Override
                    public boolean onResourceReady(Drawable resource, Object model, Target<Drawable> target, DataSource dataSource, boolean isFirstResource) {
                        supportStartPostponedEnterTransition();
                        return false;
                    }
                })
                .into(imgViewPoster);

        Glide.with(this)
                .load(Constants.BASE_BACKDROP_URL + movieDetail.getBackdropPath())
                .into(imgViewBackdrop);

        txtViewMovieTitle.setText(movieDetail.getTitle());
        String basicInfo = movieDetail.getRuntime() + " min | " + Formatter.formatDateFromDatabase(movieDetail.getReleaseDate());
        txtViewBasicInfo.setText(basicInfo);
        collapsingToolbarLayout.setTitle(movieDetail.getTitle());
        collapsingToolbarLayout.setExpandedTitleColor(getResources().getColor(android.R.color.transparent));
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_movie_detail);
        ButterKnife.bind(this);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        Bundle extras = getIntent().getExtras();

        viewPager.setAdapter(viewPagerAdapter);
        tabLayout.setupWithViewPager(viewPager);
        presenter.attachView(this);

        if (extras != null) {
            movieId = extras.getInt(EXTRA_MOVIE_ID);
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                imgViewPoster.setTransitionName(extras.getString(EXTRA_MOVIE_POSTER_TRANSITION_NAME));
                txtViewMovieTitle.setTransitionName(extras.getString(EXTRA_MOVIE_TITLE_TRANSITION_NAME));
            }
        }

        presenter.loadMovieDetails(movieId);
        viewPagerAdapter.setMovieId(movieId);
        supportPostponeEnterTransition();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.detail_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finishAfterTransition();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onDestroy() {
        presenter.detachView();
        super.onDestroy();
    }

    @Override
    public AndroidInjector<Fragment> supportFragmentInjector() {
        return fragmentInjector;
    }
}
