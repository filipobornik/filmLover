package com.android.obornik.filip.filmlover.ui.movieDetail.cast;

import android.support.v4.app.Fragment;
import com.android.obornik.filip.filmlover.common.base.BaseFragmentModule;
import com.android.obornik.filip.filmlover.core.dagger.PerFragment;
import dagger.Binds;
import dagger.Module;

import javax.inject.Named;

/**
 * Created by Filip on 1/29/2018.
 */

@Module(includes = BaseFragmentModule.class)
public abstract class CastFragmentModule {

    @Binds
    @PerFragment
    @Named(BaseFragmentModule.FRAGMENT)
    abstract Fragment provideFragment(CastFragment castFragment);

    @Binds
    @PerFragment
    @Named(BaseFragmentModule.FRAGMENT)
    abstract CastFragmentContract.View provideFragmentAsView(CastFragment castFragment);

    @Binds
    @PerFragment
    abstract CastFragmentContract.Presenter providePresenter(CastFragmentPresenter castFragmentPresenter);
}
