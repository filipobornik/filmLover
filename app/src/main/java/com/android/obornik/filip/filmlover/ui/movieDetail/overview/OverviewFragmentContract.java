package com.android.obornik.filip.filmlover.ui.movieDetail.overview;

import com.android.obornik.filip.filmlover.common.presenter.BasePresenter;
import com.android.obornik.filip.filmlover.common.view.BaseView;
import com.android.obornik.filip.filmlover.data.model.movie.MovieDetail;

public interface OverviewFragmentContract {

    interface View extends BaseView {

        void showMovieOverview(MovieDetail movieDetail);

    }

    interface Presenter extends BasePresenter<View> {

        void loadMovieOverview(int movieId);

    }

}
