package com.android.obornik.filip.filmlover.ui.castDetail.castCredits;

import com.android.obornik.filip.filmlover.core.dagger.PerFragment;
import com.android.obornik.filip.filmlover.data.DataManager;
import com.android.obornik.filip.filmlover.data.model.movie.MovieSharedElement;

import javax.inject.Inject;

/**
 * Created by Filip on 2/10/2018.
 */

@PerFragment
public class CastCreditsFragmentPresenter implements CastCreditsFragmentContract.Presenter {

    private CastCreditsFragmentContract.View view;
    private DataManager dataManager;

    @Inject
    public CastCreditsFragmentPresenter(DataManager dataManager) {
        this.dataManager = dataManager;
    }

    @Override
    public void attachView(CastCreditsFragmentContract.View view) {
        this.view = view;
    }

    @Override
    public void detachView() {
        this.view = null;
    }

    @Override
    public boolean isViewAttached() {
        return view != null;
    }

    @Override
    public void loadCastMovieCredits(int id) {
        view.showLoading();
        dataManager.getActorMovieCredit(id)
                .subscribe(actorsMovieCredits -> {
                    if (isViewAttached()) {
                        view.hideLoading();
                        view.showCastMovieCredits(actorsMovieCredits);
                   }
                });
    }

    @Override
    public void onCreditItemClicked(MovieSharedElement movieSharedElement) {
        view.showMovieDetailActivity(movieSharedElement);
    }
}
