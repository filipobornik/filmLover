package com.android.obornik.filip.filmlover.ui.movieLists.movieListFragment;

import android.content.Context;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import com.android.obornik.filip.filmlover.common.base.BaseChildFragmentModule;
import com.android.obornik.filip.filmlover.core.dagger.ApplicationContext;
import com.android.obornik.filip.filmlover.core.dagger.PerChildFragment;
import dagger.Module;
import dagger.Provides;

/**
 * Created by Filip on 12/29/2017.
 */

@Module(includes = {BaseChildFragmentModule.class, MovieListFragmentPresenterModule.class})
public class BaseMovieListFragmentModule {

    @Provides
    @PerChildFragment
    static LinearLayoutManager provideLayoutManager(@ApplicationContext Context context) {
        return new LinearLayoutManager(context, LinearLayoutManager.VERTICAL, false);
    }

    @Provides
    @PerChildFragment
    static GridLayoutManager provideGridManager(@ApplicationContext Context context) {
        return new GridLayoutManager(context, 3, GridLayoutManager.VERTICAL, false);
    }

}
