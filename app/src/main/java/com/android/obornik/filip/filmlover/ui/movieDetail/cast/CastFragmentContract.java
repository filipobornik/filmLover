package com.android.obornik.filip.filmlover.ui.movieDetail.cast;

import com.android.obornik.filip.filmlover.common.presenter.BasePresenter;
import com.android.obornik.filip.filmlover.common.view.BaseView;
import com.android.obornik.filip.filmlover.data.model.castAndCrew.Cast;

import java.util.List;

public class CastFragmentContract {

    interface View extends BaseView {

        void showCast(List<Cast> cast);

        void showActorDetail(int castId);

    }

    interface Presenter extends BasePresenter<View> {

        void loadCast(int movieId, int pages);

        void onCastItemClicked(int castId);

    }

}
