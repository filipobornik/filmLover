package com.android.obornik.filip.filmlover.ui.registerOrSignIn;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.Toast;
import com.android.obornik.filip.filmlover.R;
import com.android.obornik.filip.filmlover.data.NetworkApi;

import javax.inject.Inject;

public class RegisterOrSignInActivity extends AppCompatActivity implements RegisterOrSignInContract.View {

    @Inject NetworkApi networkApi;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sign_in);

    }

    @Override
    public void showTextMessage(String message) {
        Toast.makeText(this, message, Toast.LENGTH_SHORT).show();
    }
}
