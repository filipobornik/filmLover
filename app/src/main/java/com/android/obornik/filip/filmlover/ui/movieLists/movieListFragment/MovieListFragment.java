package com.android.obornik.filip.filmlover.ui.movieLists.movieListFragment;


import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.os.Parcelable;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.v4.app.ActivityOptionsCompat;
import android.support.v4.app.Fragment;
import android.support.v4.util.Pair;
import android.support.v4.view.ViewCompat;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import butterknife.BindView;
import butterknife.ButterKnife;
import com.android.obornik.filip.filmlover.R;
import com.android.obornik.filip.filmlover.common.base.BaseFragment;
import com.android.obornik.filip.filmlover.data.model.movie.Movie;
import com.android.obornik.filip.filmlover.data.model.movie.MovieSharedElement;
import com.android.obornik.filip.filmlover.ui.movieDetail.MovieDetailActivity;
import com.android.obornik.filip.filmlover.utils.Constants;

import javax.inject.Inject;
import java.util.List;

/**
 * A simple {@link Fragment} subclass.
 */
public class MovieListFragment extends BaseFragment<MovieListFragmentPresenter>
        implements MovieListFragmentContract.View, SwipeRefreshLayout.OnRefreshListener {

    private static final String LOG_TAG = MovieListFragment.class.getSimpleName();
    private static final String LIST_STATE_KEY = "PopularMovieListState";

    @BindView(R.id.swipeRefreshLayout) SwipeRefreshLayout swipeRefreshLayout;
    @BindView(R.id.movieListRecyclerView) RecyclerView recyclerView;
    @BindView(R.id.error_text) TextView txtViewErrorText;

    @Inject MovieListAdapter adapter;
    @Inject GridLayoutManager layoutManager;
    private Parcelable listState;
    @Constants.MovieType private int movieType;

    public MovieListFragment() {
    }

    @Override
    public void showMovies(List<Movie> movies) {
        adapter.updateMovies(movies);
        if (listState != null) {
            recyclerView.getLayoutManager().onRestoreInstanceState(listState);
        }
    }

    @Override
    public void showMovieDetailActivity(MovieSharedElement movieSharedElement) {
        View moviePosterView = movieSharedElement.getViewPosterImage();
        View movieTitleView = movieSharedElement.getViewMovieTitle();

        Intent intent = new Intent(activityContext, MovieDetailActivity.class);
        intent.putExtra(MovieDetailActivity.EXTRA_MOVIE_ID, movieSharedElement.getMovieId());

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            intent.putExtra(MovieDetailActivity.EXTRA_MOVIE_POSTER_TRANSITION_NAME, ViewCompat.getTransitionName(moviePosterView));
            intent.putExtra(MovieDetailActivity.EXTRA_MOVIE_TITLE_TRANSITION_NAME, ViewCompat.getTransitionName(movieTitleView));
            Pair<View, String> moviePosterPair = Pair.create(moviePosterView, ViewCompat.getTransitionName(moviePosterView));
            Pair<View, String> movieTitlePair = Pair.create(movieTitleView, ViewCompat.getTransitionName(movieTitleView));
            ActivityOptionsCompat options = ActivityOptionsCompat.
                    makeSceneTransitionAnimation(this.getActivity(), moviePosterPair, movieTitlePair);
            startActivity(intent, options.toBundle());
        } else {
            startActivity(intent);
        }
    }

    @Override
    public void onRefresh() {
        listState = null;
        presenter.onRefresh();
    }

    @Override
    public void showLoadingIndicator() {
        swipeRefreshLayout.setRefreshing(true);
    }

    @Override
    public void hideLoadingIndicator() {
        swipeRefreshLayout.setRefreshing(false);
    }

    @Override
    public void showNoNetworkConnectionIndicator() {
        Snackbar snackbar = Snackbar.make(getActivity().findViewById(R.id.fragment_container), R.string.no_network_connection, Snackbar.LENGTH_INDEFINITE);
        snackbar.setActionTextColor(resources.getColor(R.color.colorAccent));
        snackbar.setAction(R.string.retry, view -> presenter.onRetryButtonClicked());
        snackbar.show();
    }

    @Override
    public int getMovieListType() {
        return movieType;
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_popular_movies_list, container, false);
        ButterKnife.bind(this, view);
        recyclerView.setAdapter(adapter);
        recyclerView.setLayoutManager(layoutManager);
        swipeRefreshLayout.setOnRefreshListener(this);
        Log.d(LOG_TAG, "created recycler view with films");

        return view;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        presenter.attachView(this);
        adapter.getItemOnClickObservable().subscribe(this::showMovieDetailActivity);

        if (savedInstanceState != null) {
            listState = savedInstanceState.getParcelable(LIST_STATE_KEY);
        }

        movieType = getArguments() == null ? 1 : getArguments().getInt(Constants.MOVIE_TYPE_KEY);
        presenter.loadMovies();
    }

    @Override
    public void onSaveInstanceState(@NonNull Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putParcelable(LIST_STATE_KEY, layoutManager.onSaveInstanceState());
    }

    @Override
    public void onDestroy() {
        Log.d(LOG_TAG, "fragment has been destroyed");
        presenter.detachView();
        super.onDestroy();
    }
}
