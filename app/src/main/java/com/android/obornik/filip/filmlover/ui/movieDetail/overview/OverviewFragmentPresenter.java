package com.android.obornik.filip.filmlover.ui.movieDetail.overview;

import com.android.obornik.filip.filmlover.core.dagger.PerFragment;
import com.android.obornik.filip.filmlover.data.DataManager;
import com.android.obornik.filip.filmlover.utils.schedulers.SchedulersProvider;

import javax.inject.Inject;

/**
 * Created by Filip on 1/15/2018.
 */

@PerFragment
public class OverviewFragmentPresenter implements OverviewFragmentContract.Presenter {

    private OverviewFragmentContract.View view;
    private DataManager dataManager;
    private SchedulersProvider schedulersProvider;

    @Inject
    public OverviewFragmentPresenter(DataManager dataManager, SchedulersProvider schedulersProvider) {
        this.dataManager = dataManager;
        this.schedulersProvider = schedulersProvider;
    }

    @Override
    public void attachView(OverviewFragmentContract.View view) {
        this.view = view;
    }

    @Override
    public void detachView() {
        this.view = null;
    }

    @Override
    public boolean isViewAttached() {
        return view != null;
    }

    @Override
    public void loadMovieOverview(int movieId) {
        dataManager.getMovieDetails(movieId)
                .subscribe(movieDetail -> {
                    if (isViewAttached()) {
                        view.showMovieOverview(movieDetail);
                    }
                });
    }
}
