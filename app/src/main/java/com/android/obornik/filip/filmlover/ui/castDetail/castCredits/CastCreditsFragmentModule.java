package com.android.obornik.filip.filmlover.ui.castDetail.castCredits;

import android.support.v4.app.Fragment;
import com.android.obornik.filip.filmlover.common.base.BaseFragmentModule;
import com.android.obornik.filip.filmlover.core.dagger.PerFragment;
import dagger.Binds;
import dagger.Module;

import javax.inject.Named;

/**
 * Created by Filip on 2/10/2018.
 */

@Module(includes = BaseFragmentModule.class)
public abstract class CastCreditsFragmentModule {

    @Binds
    @PerFragment
    @Named(BaseFragmentModule.FRAGMENT)
    abstract CastCreditsFragmentContract.View provideFragmentAsView(CastCreditsFragment castCreditsFragment);

    @Binds
    @PerFragment
    @Named(BaseFragmentModule.FRAGMENT)
    abstract Fragment provideFragment(CastCreditsFragment castCreditsFragment);

    @Binds
    @PerFragment
    abstract CastCreditsFragmentContract.Presenter providePresenter(CastCreditsFragmentPresenter castCreditsFragmentPresenter);

}
